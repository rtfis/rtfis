const acknowledgeUrl = '/acknowledge';
const dishSearchTypeUrl = '/master/dish-search-type';
const ingredientSearchTypeUrl = '/master/ingredient-search-type';
const dishTypeUrl = '/master/dish-type';
const ingredientTypeUrl = '/master/ingredient-type';
const nutritionTypeUrl = '/master/nutrition-type';
const unitUrl = '/master/unit-type';
const searchGroupUrl = '/ingredient/search-group';

const ingredientListUrl = 'ingredient/all';


$(document).ready(function () {
    acknowledge();
    initialDatatable();
});


// create an element
const createNode = (elem) => {
    return document.createElement(elem);
};

// append an element to parent
const appendNode = (parent, elem) => {
    parent.appendChild(elem);
}

function acknowledge() {
    fetch(acknowledgeUrl)
        .then(res => res.text())
        .then(data => {
            if ("SUCCESS" === data) {

            } else {
                alert("ERROR");
            }
        }).catch(err => {
        console.error('Error: ', err);
        alert("ERROR")
    });
}

function getFormData($form) {
    let unindexed_array = $form.serializeArray();
    let indexed_array = {};
    $.map(unindexed_array, function (n, i) {
        indexed_array[n['name']] = n['value'];
    });
    return indexed_array;
}

function initialDatatable(obj) {
    let dataTable = $('.data-table');
    if (obj != null) {
        dataTable = obj;
    }
    dataTable.DataTable({
        scrollY: '35vh',
        scrollX: true,
        scrollCollapse: true,
        paging: true,
        autoWidth: false,
        initComplete: function () {
        }
    });
}

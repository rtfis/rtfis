// RELOAD
function reloadIngredientContent() {
    $.get("reload-ingredient").done(function (fragment) {
        $("#ingredient-content").html(fragment);
        initialDatatable($("#ingredient"));
    });
}

// MODAL
function showModalViewIngredient(ingredientId) {
    openModal("modal-ingredient-view/" + ingredientId, "View Thai Dish");
}

function showModalEditIngredient(ingredientId) {
    openModal("modal-ingredient-edit/" + ingredientId, "Edit Ingredient");
}


// FORM-SUBMIT
function createIngredientSubmit(elm) {
    const url = 'ingredient/add';
    let data = new FormData(elm);
    fetch(url, {
        method: 'POST',
        body: data,
    }).then(response => response.text())
        .then(data => {
            if ("SUCCESS" == data) {
                toastr.success("ADD INGREDIENT SUCCESS");
            } else {
                toastr.error("ADD INGREDIENT FAIL");
            }
        })
        .finally(function () {
            closeModal();
            reloadIngredientContent();
        });
}

function editIngredientSubmit(elm) {
    const url = 'ingredient/update';
    let data = new FormData(elm);
    fetch(url, {
        method: 'PUT',
        body: data,
    }).then(response => response.text())
        .then(data => {
            if ("SUCCESS" == data) {
                toastr.success("EDIT INGREDIENT SUCCESS");
            } else {
                toastr.error("EDIT INGREDIENT FAIL");
            }
        })
        .finally(function () {
            closeModal();
            reloadIngredientContent();
        });
}

function deleteIngredientSubmit(id) {
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            fetch("ingredient/delete/" + id, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                },
            }).then(response => response.text())
                .then(data => {
                    if ("SUCCESS" == data) {
                        toastr.success("DELETE INGREDIENT SUCCESS");
                    } else {
                        toastr.error("DELETE INGREDIENT FAIL");
                    }
                })
                .finally(function () {
                    reloadIngredientContent();
                });
        }
    });
}
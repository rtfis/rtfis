// RELOAD
function reloadNutritionContent() {
    $.get("reload-nutrition").done(function (fragment) {
        $("#nutrition-content").html(fragment);
        initialDatatable($("#nutrition"));
    });
}

// MODAL
function showModalViewNutrition(nutritionId) {
    openModal("modal-nutrition-view/" + nutritionId, "View Nutrition");
}

function showModalEditNutrition(nutritionId) {
    openModal("modal-nutrition-edit/" + nutritionId, "Edit Nutrition");
}


// FORM-SUBMIT
function createNutritionSubmit(elm) {
    const url = 'nutrition/add';
    let data = new FormData(elm);
    fetch(url, {
        method: 'POST',
        body: data,
    }).then(response => response.text())
        .then(data => {
            if ("SUCCESS" == data) {
                toastr.success("ADD NUTRITION SUCCESS");
            } else {
                toastr.error("ADD NUTRITION FAIL");
            }
        })
        .finally(function () {
            closeModal();
            reloadNutritionContent();
        });
}

function editNutritionSubmit(elm) {
    const url = 'nutrition/update';
    let data = new FormData(elm);
    fetch(url, {
        method: 'PUT',
        body: data,
    }).then(response => response.text())
        .then(data => {
            if ("SUCCESS" == data) {
                toastr.success("EDIT NUTRITION SUCCESS");
            } else {
                toastr.error("EDIT NUTRITION FAIL");
            }
        })
        .finally(function () {
            closeModal();
            reloadNutritionContent();
        });
}

function deleteNutritionSubmit(id) {
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            fetch("nutrition/delete/" + id, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                },
            }).then(response => response.text())
                .then(data => {
                    if ("SUCCESS" == data) {
                        toastr.success("DELETE NUTRITION SUCCESS");
                    } else {
                        toastr.error("DELETE NUTRITION FAIL");
                    }
                })
                .finally(function () {
                    reloadNutritionContent();
                });
        }
    });
}
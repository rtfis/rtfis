// RELOAD
function reloadThaiDishContent() {
    $.get("reload-thaidish").done(function (fragment) {
        $("#thai-dish-maintain-content").html(fragment);
        initialDatatable($("#thai-dish-maintain"));
    });
}

// MODAL
function showModalViewThaiDish(dishId) {
    openModal("modal-thai-dish-view/" + dishId, "View Thai Dish");
}

function showModalEditThaiDish(dishId) {
    openModal("modal-thai-dish-edit/" + dishId, "Edit Thai Dish");
}

// FORM-SUBMIT
function createThaiDishSubmit(elm) {
    const url = 'dish/add';
    let data = new FormData(elm);
    fetch(url, {
        method: 'POST',
        body: data,
    }).then(response => response.text())
        .then(data => {
            if ("SUCCESS" == data) {
                toastr.success("ADD DISH SUCCESS");
                toastr.info("DISH WAITING FOR APPROVE");
            } else {
                toastr.error("ADD DISH FAIL");
            }
        })
        .finally(function () {
            closeModal();
            reloadThaiDishContent();
        });
}

function editThaiDishSubmit(elm) {
    const url = 'dish/update';
    let data = new FormData(elm);
    fetch(url, {
        method: 'PUT',
        body: data,
    }).then(response => response.text())
        .then(data => {
            if ("SUCCESS" == data) {
                toastr.success("EDIT DISH SUCCESS");
            } else {
                toastr.error("EDIT DISH FAIL");
            }
        })
        .finally(function () {
            closeModal();
            reloadThaiDishContent();
        });
}

function deleteThaiDishSubmit(id) {
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.isConfirmed) {
            fetch("dish/delete/" + id, {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                },
            }).then(response => response.text())
                .then(data => {
                    if ("SUCCESS" == data) {
                        toastr.success("DELETE DISH SUCCESS");
                    } else {
                        toastr.error("DELETE DISH FAIL");
                    }
                })
                .finally(function () {
                    reloadThaiDishContent();
                });
        }
    });
}
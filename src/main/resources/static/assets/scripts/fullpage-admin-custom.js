var fullpage = new fullpage('#fullpage', {
    anchors: ['dish-review', 'dish-maintain'],
    sectionsColor: ['', '#FDF2E9'],
    navigation: true,
    navigationPosition: 'right',
    responsiveWidth: 900,
    menu: '#menu',
    afterResponsive: function (isResponsive) {

    },
    fitToSection: false,
    autoScrolling: false
});
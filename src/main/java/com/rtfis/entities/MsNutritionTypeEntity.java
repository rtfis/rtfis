package com.rtfis.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "ms_nutrition_type")
public class MsNutritionTypeEntity {
    @Id
    @Column(name = "nutrition_type_id")
    private int nutritionTypeId;
    @Column(name = "nutrition_type_name_th")
    private String nutritionTypeNameTh;
    @Column(name = "nutrition_type_name_en")
    private String nutritionTypeNameEn;

    public int getNutritionTypeId() {
        return nutritionTypeId;
    }

    public void setNutritionTypeId(int nutritionTypeId) {
        this.nutritionTypeId = nutritionTypeId;
    }

    public String getNutritionTypeNameTh() {
        return nutritionTypeNameTh;
    }

    public void setNutritionTypeNameTh(String nutritionTypeNameTh) {
        this.nutritionTypeNameTh = nutritionTypeNameTh;
    }

    public String getNutritionTypeNameEn() {
        return nutritionTypeNameEn;
    }

    public void setNutritionTypeNameEn(String nutritionTypeNameEn) {
        this.nutritionTypeNameEn = nutritionTypeNameEn;
    }

    @Override
    public String toString() {
        return "MsNutritionTypeEntity{" +
                "nutritionTypeId=" + nutritionTypeId +
                ", nutritionTypeNameTh='" + nutritionTypeNameTh + '\'' +
                ", nutritionTypeNameEn='" + nutritionTypeNameEn + '\'' +
                '}';
    }
}

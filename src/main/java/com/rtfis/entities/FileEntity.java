package com.rtfis.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

@Entity
@Table(name = "file")
public class FileEntity implements Serializable {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "file_id")
    private int fileId;

    @Column(name = "file_group")
    private String fileGroup;

    @Column(name = "file_name")
    private String fileName;

    @Column(name = "file_size")
    private int fileSize;

    @Column(name = "file_type")
    private String fileType;

    @Column(name = "file_store_type")
    private String fileStoreType;

    @Column(name = "file_path")
    private String filePath;

    @Column(name = "file_download_url")
    private String fileDownloadUrl;

    @Column(name = "file_create_date")
    private Timestamp fileCreateDate;

    @Column(name = "file_create_by")
    private String fileCreateBy;

    @Column(name = "file_update_date")
    private Timestamp fileUpdateDate;

    @Column(name = "file_update_by")
    private String fileUpdateBy;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "file")
    @JsonBackReference
    private DishEntity dish;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "file")
    @JsonBackReference
    private IngredientEntity ingredient;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "file")
    @JsonBackReference
    private NutritionEntity nutrition;

    public FileEntity(String fileName, String fileType) {
        this.fileName = fileName;
        this.fileType = fileType;
    }


    public FileEntity() {

    }

    public int getFileId() {
        return fileId;
    }

    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    public String getFileGroup() {
        return fileGroup;
    }

    public void setFileGroup(String fileGroup) {
        this.fileGroup = fileGroup;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public int getFileSize() {
        return fileSize;
    }

    public void setFileSize(int fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileType() {
        return fileType;
    }

    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    public String getFileStoreType() {
        return fileStoreType;
    }

    public void setFileStoreType(String fileStoreType) {
        this.fileStoreType = fileStoreType;
    }

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getFileDownloadUrl() {
        return fileDownloadUrl;
    }

    public void setFileDownloadUrl(String fileDownloadUrl) {
        this.fileDownloadUrl = fileDownloadUrl;
    }

    public Timestamp getFileCreateDate() {
        return fileCreateDate;
    }

    public void setFileCreateDate(Timestamp fileCreateDate) {
        this.fileCreateDate = fileCreateDate;
    }

    public String getFileCreateBy() {
        return fileCreateBy;
    }

    public void setFileCreateBy(String fileCreateBy) {
        this.fileCreateBy = fileCreateBy;
    }

    public Timestamp getFileUpdateDate() {
        return fileUpdateDate;
    }

    public void setFileUpdateDate(Timestamp fileUpdateDate) {
        this.fileUpdateDate = fileUpdateDate;
    }

    public String getFileUpdateBy() {
        return fileUpdateBy;
    }

    public void setFileUpdateBy(String fileUpdateBy) {
        this.fileUpdateBy = fileUpdateBy;
    }

    public DishEntity getDish() {
        return dish;
    }

    public void setDish(DishEntity dish) {
        this.dish = dish;
    }

    public IngredientEntity getIngredient() {
        return ingredient;
    }

    public void setIngredient(IngredientEntity ingredient) {
        this.ingredient = ingredient;
    }

    public NutritionEntity getNutrition() {
        return nutrition;
    }

    public void setNutrition(NutritionEntity nutrition) {
        this.nutrition = nutrition;
    }
}

package com.rtfis.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

@Entity
@Table(name = "ms_unit_type")
public class MsUnitTypeEntity {
    @Id
    @Column(name = "unit_id")
    private int unitId;

    @Column(name = "unit_name_th")
    private String unitNameTh;

    @Column(name = "unit_name_en")
    private String unitNameEn;

//    @OneToOne(mappedBy = "unit",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
//    @JsonBackReference
//    private CookIngredientEntity cookIngredient;
//
//    @OneToOne(mappedBy = "unit",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
//    @JsonBackReference
//    private IngredientSubstitutionEntity ingredientSubstitution;

    public int getUnitId() {
        return unitId;
    }

    public void setUnitId(int unitId) {
        this.unitId = unitId;
    }

    public String getUnitNameTh() {
        return unitNameTh;
    }

    public void setUnitNameTh(String unitNameTh) {
        this.unitNameTh = unitNameTh;
    }

    public String getUnitNameEn() {
        return unitNameEn;
    }

    public void setUnitNameEn(String unitNameEn) {
        this.unitNameEn = unitNameEn;
    }

//    public CookIngredientEntity getCookIngredient() {
//        return cookIngredient;
//    }
//
//    public void setCookIngredient(CookIngredientEntity cookIngredient) {
//        this.cookIngredient = cookIngredient;
//    }
//
//    public IngredientSubstitutionEntity getIngredientSubstitution() {
//        return ingredientSubstitution;
//    }
//
//    public void setIngredientSubstitution(IngredientSubstitutionEntity ingredientSubstitution) {
//        this.ingredientSubstitution = ingredientSubstitution;
//    }

    @Override
    public String toString() {
        return "MsUnitTypeEntity{" +
                "unitId=" + unitId +
                ", unitNameTh='" + unitNameTh + '\'' +
                ", unitNameEn='" + unitNameEn + '\'' +
//                ", cookIngredient=" + cookIngredient +
//                ", ingredientSubstitution=" + ingredientSubstitution +
                '}';
    }
}

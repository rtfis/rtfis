package com.rtfis.entities;

import javax.persistence.*;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "nutrition")
public class NutritionEntity {
    @Id
    @Column(name = "nutrition_id")
    private int nutritionId;
    @Column(name = "nutrition_type_id")
    private int nutritionTypeId;
    @Column(name = "nutrition_name_th")
    private String nutritionNameTh;
    @Column(name = "nutrition_detail_th")
    private String nutritionDetailTh;
    @Column(name = "nutrition_name_en")
    private String nutritionNameEn;
    @Column(name = "nutrition_detail_en")
    private String nutritionDetailEn;

    @OneToOne(fetch = FetchType.EAGER,orphanRemoval = true)
    @JoinColumn(name = "file_id")
    private FileEntity file;

    @Column(name = "nutrition_create_date")
    private Date nutritionCreateDate;

    @Column(name = "nutrition_create_by")
    private String nutritionCreateBy;

    @Column(name = "nutrition_update_date")
    private Timestamp nutritionUpdateDate;

    @Column(name = "nutrition_update_by")
    private String nutritionUpdateBy;

    public int getNutritionId() {
        return nutritionId;
    }

    public void setNutritionId(int nutritionId) {
        this.nutritionId = nutritionId;
    }

    public int getNutritionTypeId() {
        return nutritionTypeId;
    }

    public void setNutritionTypeId(int nutritionTypeId) {
        this.nutritionTypeId = nutritionTypeId;
    }

    public String getNutritionNameTh() {
        return nutritionNameTh;
    }

    public void setNutritionNameTh(String nutritionNameTh) {
        this.nutritionNameTh = nutritionNameTh;
    }

    public String getNutritionDetailTh() {
        return nutritionDetailTh;
    }

    public void setNutritionDetailTh(String nutritionDetailTh) {
        this.nutritionDetailTh = nutritionDetailTh;
    }

    public String getNutritionNameEn() {
        return nutritionNameEn;
    }

    public void setNutritionNameEn(String nutritionNameEn) {
        this.nutritionNameEn = nutritionNameEn;
    }

    public String getNutritionDetailEn() {
        return nutritionDetailEn;
    }

    public void setNutritionDetailEn(String nutritionDetailEn) {
        this.nutritionDetailEn = nutritionDetailEn;
    }

    public FileEntity getFile() {
        return file;
    }

    public void setFile(FileEntity file) {
        this.file = file;
    }

    public Date getNutritionCreateDate() {
        return nutritionCreateDate;
    }

    public void setNutritionCreateDate(Date nutritionCreateDate) {
        this.nutritionCreateDate = nutritionCreateDate;
    }

    public String getNutritionCreateBy() {
        return nutritionCreateBy;
    }

    public void setNutritionCreateBy(String nutritionCreateBy) {
        this.nutritionCreateBy = nutritionCreateBy;
    }

    public Timestamp getNutritionUpdateDate() {
        return nutritionUpdateDate;
    }

    public void setNutritionUpdateDate(Timestamp nutritionUpdateDate) {
        this.nutritionUpdateDate = nutritionUpdateDate;
    }

    public String getNutritionUpdateBy() {
        return nutritionUpdateBy;
    }

    public void setNutritionUpdateBy(String nutritionUpdateBy) {
        this.nutritionUpdateBy = nutritionUpdateBy;
    }

    @Override
    public String toString() {
        return "NutritionEntity{" +
                "nutritionId=" + nutritionId +
                ", nutritionTypeId=" + nutritionTypeId +
                ", nutritionNameTh='" + nutritionNameTh + '\'' +
                ", nutritionDetailTh='" + nutritionDetailTh + '\'' +
                ", nutritionNameEn='" + nutritionNameEn + '\'' +
                ", nutritionDetailEn='" + nutritionDetailEn + '\'' +
                ", file=" + file +
                ", nutritionCreateDate=" + nutritionCreateDate +
                ", nutritionCreateBy='" + nutritionCreateBy + '\'' +
                ", nutritionUpdateDate=" + nutritionUpdateDate +
                ", nutritionUpdateBy='" + nutritionUpdateBy + '\'' +
                '}';
    }
}

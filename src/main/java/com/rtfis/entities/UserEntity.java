package com.rtfis.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "user")
public class UserEntity {
    private int userId;
    private String userEmail;
    private String userName;
    private String userLastName;
    private String userPassword;
    private String userActivatedFlag;
    private Timestamp userCreateDate;
    private String userCreateBy;
    private Timestamp userUpdateDate;
    private String userUpdateBy;
    private int fileId;
    private int userRoleId;
    private FileEntity fileByFileId;

    @Id
    @Column(name = "user_id")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "user_email")
    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    @Basic
    @Column(name = "user_name")
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Basic
    @Column(name = "user_last_name")
    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    @Basic
    @Column(name = "user_password")
    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    @Basic
    @Column(name = "user_activated_flag")
    public String getUserActivatedFlag() {
        return userActivatedFlag;
    }

    public void setUserActivatedFlag(String userActivatedFlag) {
        this.userActivatedFlag = userActivatedFlag;
    }

    @Basic
    @Column(name = "user_create_date")
    public Timestamp getUserCreateDate() {
        return userCreateDate;
    }

    public void setUserCreateDate(Timestamp userCreateDate) {
        this.userCreateDate = userCreateDate;
    }

    @Basic
    @Column(name = "user_create_by")
    public String getUserCreateBy() {
        return userCreateBy;
    }

    public void setUserCreateBy(String userCreateBy) {
        this.userCreateBy = userCreateBy;
    }

    @Basic
    @Column(name = "user_update_date")
    public Timestamp getUserUpdateDate() {
        return userUpdateDate;
    }

    public void setUserUpdateDate(Timestamp userUpdateDate) {
        this.userUpdateDate = userUpdateDate;
    }

    @Basic
    @Column(name = "user_update_by")
    public String getUserUpdateBy() {
        return userUpdateBy;
    }

    public void setUserUpdateBy(String userUpdateBy) {
        this.userUpdateBy = userUpdateBy;
    }

    @Basic
    @Column(name = "file_id")
    public int getFileId() {
        return fileId;
    }

    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    @Basic
    @Column(name = "user_role_id")
    public int getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(int userRoleId) {
        this.userRoleId = userRoleId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserEntity that = (UserEntity) o;
        return userId == that.userId &&
                Objects.equals(userEmail, that.userEmail) &&
                Objects.equals(userName, that.userName) &&
                Objects.equals(userLastName, that.userLastName) &&
                Objects.equals(userPassword, that.userPassword) &&
                Objects.equals(userActivatedFlag, that.userActivatedFlag) &&
                Objects.equals(userCreateDate, that.userCreateDate) &&
                Objects.equals(userCreateBy, that.userCreateBy) &&
                Objects.equals(userUpdateDate, that.userUpdateDate) &&
                Objects.equals(userUpdateBy, that.userUpdateBy) &&
                Objects.equals(fileId, that.fileId) &&
                Objects.equals(userRoleId, that.userRoleId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userId, userEmail, userName, userLastName, userPassword, userActivatedFlag, userCreateDate, userCreateBy, userUpdateDate, userUpdateBy, fileId, userRoleId);
    }

    @ManyToOne
    @JoinColumn(name = "file_id", referencedColumnName = "file_id", table = "user",insertable = false,updatable = false)
    public FileEntity getFileByFileId() {
        return fileByFileId;
    }

    public void setFileByFileId(FileEntity fileByFileId) {
        this.fileByFileId = fileByFileId;
    }
}

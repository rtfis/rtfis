package com.rtfis.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "ingredient_substitution")
public class IngredientSubstitutionEntity implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "ingredient_substitution_id")
    private Integer ingredientSubstitutionId;

    @ManyToOne
    @JoinColumn(name="main_ingredient_id", nullable=false)
    @JsonBackReference
    private IngredientEntity mainIngredient;

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name="substitution_id", nullable=false)
    @JsonBackReference
    private IngredientEntity subIngredient;

    @ManyToOne
    @JoinColumn(name = "cook_ingredient_id", nullable=false)
    @JsonBackReference
    private CookIngredientEntity  cookIngredient;

    @Column(name = "cook_ingredient_amount")
    private String cookIngredientAmount;

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "cook_ingredient_unit_id")
    @JsonIgnore
    private MsUnitTypeEntity unit;

    public Integer getIngredientSubstitutionId() {
        return ingredientSubstitutionId;
    }

    public void setIngredientSubstitutionId(Integer ingredientSubstitutionId) {
        this.ingredientSubstitutionId = ingredientSubstitutionId;
    }

    public IngredientEntity getMainIngredient() {
        return mainIngredient;
    }

    public void setMainIngredient(IngredientEntity mainIngredient) {
        this.mainIngredient = mainIngredient;
    }

    public IngredientEntity getSubIngredient() {
        return subIngredient;
    }

    public void setSubIngredient(IngredientEntity subIngredient) {
        this.subIngredient = subIngredient;
    }

    public CookIngredientEntity getCookIngredient() {
        return cookIngredient;
    }

    public void setCookIngredient(CookIngredientEntity cookIngredient) {
        this.cookIngredient = cookIngredient;
    }

    public String getCookIngredientAmount() {
        return cookIngredientAmount;
    }

    public void setCookIngredientAmount(String cookIngredientAmount) {
        this.cookIngredientAmount = cookIngredientAmount;
    }

    public MsUnitTypeEntity getUnit() {
        return unit;
    }

    public void setUnit(MsUnitTypeEntity unit) {
        this.unit = unit;
    }

    @Override
    public String toString() {
        return "IngredientSubstitutionEntity{" +
                "ingredientSubstitutionId=" + ingredientSubstitutionId +
                ", mainIngredient=" + mainIngredient +
                ", subIngredient=" + subIngredient +
                ", cookIngredient=" + cookIngredient +
                ", cookIngredientAmount='" + cookIngredientAmount + '\'' +
                ", unit=" + unit +
                '}';
    }
}

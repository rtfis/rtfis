package com.rtfis.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@Table(name = "cook_ingredient")
public class CookIngredientEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cook_ingredient_id")
    private Integer cookIngredientId;

    @Column(name = "cook_ingredient_is_main_flag")
    private String cookIngredientIsMainFlag;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "ingredient_id")
    private IngredientEntity ingredient;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "cook_id")
    @JsonBackReference
    private CookEntity cook;

    @Column(name = "cook_ingredient_amount")
    private String cookIngredientAmount;

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JoinColumn(name = "unit_id")
    private MsUnitTypeEntity unit;

    @OneToMany(mappedBy = "cookIngredient",cascade = CascadeType.ALL,fetch = FetchType.LAZY,orphanRemoval = true)
    @JsonBackReference
    private List<IngredientSubstitutionEntity> ingredientSubstitutions;

    public Integer getCookIngredientId() {
        return cookIngredientId;
    }

    public void setCookIngredientId(Integer cookIngredientId) {
        this.cookIngredientId = cookIngredientId;
    }

    public String getCookIngredientIsMainFlag() {
        return cookIngredientIsMainFlag;
    }

    public void setCookIngredientIsMainFlag(String cookIngredientIsMainFlag) {
        this.cookIngredientIsMainFlag = cookIngredientIsMainFlag;
    }

    public IngredientEntity getIngredient() {
        return ingredient;
    }

    public void setIngredient(IngredientEntity ingredient) {
        this.ingredient = ingredient;
    }

    public CookEntity getCook() {
        return cook;
    }

    public void setCook(CookEntity cook) {
        this.cook = cook;
    }

    public String getCookIngredientAmount() {
        return cookIngredientAmount;
    }

    public void setCookIngredientAmount(String cookIngredientAmount) {
        this.cookIngredientAmount = cookIngredientAmount;
    }

    public MsUnitTypeEntity getUnit() {
        return unit;
    }

    public void setUnit(MsUnitTypeEntity unit) {
        this.unit = unit;
    }

    public List<IngredientSubstitutionEntity> getIngredientSubstitutions() {
        return ingredientSubstitutions;
    }

    public void setIngredientSubstitutions(List<IngredientSubstitutionEntity> ingredientSubstitutions) {
        this.ingredientSubstitutions = ingredientSubstitutions;
    }
}

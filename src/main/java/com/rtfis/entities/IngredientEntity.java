package com.rtfis.entities;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.util.List;
import java.util.Set;

@Entity
@Table(name = "ingredient")
public class IngredientEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ingredient_id")
    private int ingredientId;
    @Column(name = "ingredient_name_th")
    private String ingredientNameTh;
    @Column(name = "ingredient_name_en")
    private String ingredientNameEn;
    @Column(name = "ingredient_detail_th")
    private String ingredientDetailTh;
    @Column(name = "ingredient_detail_en")
    private String ingredientDetailEn;
    @Column(name = "ingredient_type_id")
    private int ingredientTypeId;
    @Column(name = "ingredient_create_date")
    private Date ingredientCreateDate;
    @Column(name = "ingredient_create_by")
    private String ingredientCreateBy;
    @Column(name = "ingredient_update_date")
    private Date ingredientUpdateDate;
    @Column(name = "ingredient_update_by")
    private String ingredientUpdateBy;

    @OneToOne(cascade = CascadeType.ALL,fetch = FetchType.LAZY,orphanRemoval = true)
    @JoinColumn(name = "file_id")
    @JsonIgnore
    private FileEntity file;


    @ManyToMany
    @JoinTable(
            name = "ingredient_nutrition",
            joinColumns = @JoinColumn(name = "ingredient_id"),
            inverseJoinColumns = @JoinColumn(name = "nutrition_id"))
    @JsonBackReference
    private Set<NutritionEntity> nutritions;

    @OneToMany(mappedBy = "ingredient",cascade = CascadeType.ALL,fetch = FetchType.EAGER)
    @JsonBackReference
    private List<CookIngredientEntity> cookIngredient;


    @OneToMany(mappedBy = "mainIngredient",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    private List<IngredientSubstitutionEntity> ingredientSubstitutions;

    @OneToOne(mappedBy = "subIngredient",cascade = CascadeType.ALL,fetch = FetchType.LAZY)
    @JsonBackReference
    private IngredientSubstitutionEntity ingredientSubstitution;

    public int getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(int ingredientId) {
        this.ingredientId = ingredientId;
    }

    public String getIngredientNameTh() {
        return ingredientNameTh;
    }

    public void setIngredientNameTh(String ingredientNameTh) {
        this.ingredientNameTh = ingredientNameTh;
    }

    public String getIngredientNameEn() {
        return ingredientNameEn;
    }

    public void setIngredientNameEn(String ingredientNameEn) {
        this.ingredientNameEn = ingredientNameEn;
    }

    public String getIngredientDetailTh() {
        return ingredientDetailTh;
    }

    public void setIngredientDetailTh(String ingredientDetailTh) {
        this.ingredientDetailTh = ingredientDetailTh;
    }

    public String getIngredientDetailEn() {
        return ingredientDetailEn;
    }

    public void setIngredientDetailEn(String ingredientDetailEn) {
        this.ingredientDetailEn = ingredientDetailEn;
    }

    public int getIngredientTypeId() {
        return ingredientTypeId;
    }

    public void setIngredientTypeId(int ingredientTypeId) {
        this.ingredientTypeId = ingredientTypeId;
    }

    public Date getIngredientCreateDate() {
        return ingredientCreateDate;
    }

    public void setIngredientCreateDate(Date ingredientCreateDate) {
        this.ingredientCreateDate = ingredientCreateDate;
    }

    public String getIngredientCreateBy() {
        return ingredientCreateBy;
    }

    public void setIngredientCreateBy(String ingredientCreateBy) {
        this.ingredientCreateBy = ingredientCreateBy;
    }

    public Date getIngredientUpdateDate() {
        return ingredientUpdateDate;
    }

    public void setIngredientUpdateDate(Date ingredientUpdateDate) {
        this.ingredientUpdateDate = ingredientUpdateDate;
    }

    public String getIngredientUpdateBy() {
        return ingredientUpdateBy;
    }

    public void setIngredientUpdateBy(String ingredientUpdateBy) {
        this.ingredientUpdateBy = ingredientUpdateBy;
    }

    public FileEntity getFile() {
        return file;
    }

    public void setFile(FileEntity file) {
        this.file = file;
    }

    public Set<NutritionEntity> getNutritions() {
        return nutritions;
    }

    public void setNutritions(Set<NutritionEntity> nutritions) {
        this.nutritions = nutritions;
    }

    public List<CookIngredientEntity> getCookIngredient() {
        return cookIngredient;
    }

    public void setCookIngredient(List<CookIngredientEntity> cookIngredient) {
        this.cookIngredient = cookIngredient;
    }

    public List<IngredientSubstitutionEntity> getIngredientSubstitutions() {
        return ingredientSubstitutions;
    }

    public void setIngredientSubstitutions(List<IngredientSubstitutionEntity> ingredientSubstitutions) {
        this.ingredientSubstitutions = ingredientSubstitutions;
    }

    public IngredientSubstitutionEntity getIngredientSubstitution() {
        return ingredientSubstitution;
    }

    public void setIngredientSubstitution(IngredientSubstitutionEntity ingredientSubstitution) {
        this.ingredientSubstitution = ingredientSubstitution;
    }

    @Override
    public String toString() {
        return "IngredientEntity{" +
                "ingredientId=" + ingredientId +
                ", ingredientNameTh='" + ingredientNameTh + '\'' +
                ", ingredientNameEn='" + ingredientNameEn + '\'' +
                ", ingredientDetailTh='" + ingredientDetailTh + '\'' +
                ", ingredientDetailEn='" + ingredientDetailEn + '\'' +
                ", ingredientTypeId=" + ingredientTypeId +
                ", ingredientCreateDate=" + ingredientCreateDate +
                ", ingredientCreateBy='" + ingredientCreateBy + '\'' +
                ", ingredientUpdateDate=" + ingredientUpdateDate +
                ", ingredientUpdateBy='" + ingredientUpdateBy + '\'' +
                ", file=" + file +
                ", nutritions=" + nutritions +
                ", cookIngredient=" + cookIngredient +
                ", ingredientSubstitutions=" + ingredientSubstitutions +
                ", ingredientSubstitution=" + ingredientSubstitution +
                '}';
    }
}


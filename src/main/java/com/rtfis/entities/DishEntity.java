package com.rtfis.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

@Entity
@Table(name = "dish")
public class DishEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "dish_id")
    private int dishId;

    @Column(name = "dish_name_th")
    private String dishNameTh;

    @Column(name = "dish_name_en")
    private String dishNameEn;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(name = "dish_type_id")
    @JsonIgnore
    private MsDishTypeEntity dishType;

    @Column(name = "dish_description_th")
    private String dishDescriptionTh;

    @Column(name = "dish_description_en")
    private String dishDescriptionEn;

    @Column(name = "dish_create_date")
    private Date dishCreateDate;

    @Column(name = "dish_create_by")
    private String dishCreateBy;

    @Column(name = "dish_update_date")
    private Timestamp dishUpdateDate;

    @Column(name = "dish_update_by")
    private String dishUpdateBy;

    @Column(name = "dish_approve_flag")
    private String dishApproveFlag;

    @Column(name = "dish_approve_date")
    private Timestamp dishApproveDate;

    @Column(name = "dish_approve_by")
    private String dishApproveBy;


    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    @JoinColumn(name = "file_id")
    @JsonIgnore
    private FileEntity file;

    @OneToMany(mappedBy = "dish", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
    private List<CookEntity> cooks;

    public int getDishId() {
        return dishId;
    }

    public void setDishId(int dishId) {
        this.dishId = dishId;
    }

    public String getDishNameTh() {
        return dishNameTh;
    }

    public void setDishNameTh(String dishNameTh) {
        this.dishNameTh = dishNameTh;
    }

    public String getDishNameEn() {
        return dishNameEn;
    }

    public void setDishNameEn(String dishNameEn) {
        this.dishNameEn = dishNameEn;
    }

    public MsDishTypeEntity getDishType() {
        return dishType;
    }

    public void setDishType(MsDishTypeEntity dishType) {
        this.dishType = dishType;
    }

    public String getDishDescriptionTh() {
        return dishDescriptionTh;
    }

    public void setDishDescriptionTh(String dishDescriptionTh) {
        this.dishDescriptionTh = dishDescriptionTh;
    }

    public String getDishDescriptionEn() {
        return dishDescriptionEn;
    }

    public void setDishDescriptionEn(String dishDescriptionEn) {
        this.dishDescriptionEn = dishDescriptionEn;
    }

    public Date getDishCreateDate() {
        return dishCreateDate;
    }

    public void setDishCreateDate(Date dishCreateDate) {
        this.dishCreateDate = dishCreateDate;
    }

    public String getDishCreateBy() {
        return dishCreateBy;
    }

    public void setDishCreateBy(String dishCreateBy) {
        this.dishCreateBy = dishCreateBy;
    }

    public Timestamp getDishUpdateDate() {
        return dishUpdateDate;
    }

    public void setDishUpdateDate(Timestamp dishUpdateDate) {
        this.dishUpdateDate = dishUpdateDate;
    }

    public String getDishUpdateBy() {
        return dishUpdateBy;
    }

    public void setDishUpdateBy(String dishUpdateBy) {
        this.dishUpdateBy = dishUpdateBy;
    }

    public String getDishApproveFlag() {
        return dishApproveFlag;
    }

    public void setDishApproveFlag(String dishApproveFlag) {
        this.dishApproveFlag = dishApproveFlag;
    }

    public Timestamp getDishApproveDate() {
        return dishApproveDate;
    }

    public void setDishApproveDate(Timestamp dishApproveDate) {
        this.dishApproveDate = dishApproveDate;
    }

    public String getDishApproveBy() {
        return dishApproveBy;
    }

    public void setDishApproveBy(String dishApproveBy) {
        this.dishApproveBy = dishApproveBy;
    }

    public FileEntity getFile() {
        return file;
    }

    public void setFile(FileEntity file) {
        this.file = file;
    }

    public List<CookEntity> getCooks() {
        return cooks;
    }

    public void setCooks(List<CookEntity> cooks) {
        this.cooks = cooks;
    }
}

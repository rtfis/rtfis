package com.rtfis.entities;

import javax.persistence.*;

@Entity
@Table(name = "ms_ingredient_type")
public class MsIngredientTypeEntity {
    @Id
    @Column(name = "ingredient_type_id")
    private int ingredientTypeId;
    @Column(name = "ingredient_type_name_th")
    private String ingredientTypeNameTh;
    @Column(name = "ingredient_type_name_en")
    private String ingredientTypeNameEn;

    public int getIngredientTypeId() {
        return ingredientTypeId;
    }

    public void setIngredientTypeId(int ingredientTypeId) {
        this.ingredientTypeId = ingredientTypeId;
    }

    public String getIngredientTypeNameTh() {
        return ingredientTypeNameTh;
    }

    public void setIngredientTypeNameTh(String ingredientTypeNameTh) {
        this.ingredientTypeNameTh = ingredientTypeNameTh;
    }

    public String getIngredientTypeNameEn() {
        return ingredientTypeNameEn;
    }

    public void setIngredientTypeNameEn(String ingredientTypeNameEn) {
        this.ingredientTypeNameEn = ingredientTypeNameEn;
    }

    @Override
    public String toString() {
        return "MsIngredientTypeEntity{" +
                "ingredientTypeId=" + ingredientTypeId +
                ", ingredientTypeNameTh='" + ingredientTypeNameTh + '\'' +
                ", ingredientTypeNameEn='" + ingredientTypeNameEn + '\'' +
                '}';
    }
}

package com.rtfis.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "ms_dish_type")
public class MsDishTypeEntity implements Serializable {
    @Id
    @Column(name = "dish_type_id")
    private int dishTypeId;
    @Column(name = "dish_type_name_th")
    private String dishTypeNameTh;
    @Column(name = "dish_type_name_en")
    private String dishTypeNameEn;

//    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "dishType")
//    @JsonBackReference
//    private DishEntity dish;

    public int getDishTypeId() {
        return dishTypeId;
    }

    public void setDishTypeId(int dishTypeId) {
        this.dishTypeId = dishTypeId;
    }


    public String getDishTypeNameTh() {
        return dishTypeNameTh;
    }

    public void setDishTypeNameTh(String dishTypeName) {
        this.dishTypeNameTh = dishTypeName;
    }

    public String getDishTypeNameEn() {
        return dishTypeNameEn;
    }

    public void setDishTypeNameEn(String dishTypeNameEn) {
        this.dishTypeNameEn = dishTypeNameEn;
    }

//    public DishEntity getDish() {
//        return dish;
//    }
//
//    public void setDish(DishEntity dish) {
//        this.dish = dish;
//    }

    @Override
    public String toString() {
        return "MsDishTypeEntity{" +
                "dishTypeId=" + dishTypeId +
                ", dishTypeNameTh='" + dishTypeNameTh + '\'' +
                ", dishTypeNameEn='" + dishTypeNameEn + '\'' +
                //", dish=" + dish +
                '}';
    }
}

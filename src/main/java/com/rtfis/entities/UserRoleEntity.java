package com.rtfis.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "user_role")
public class UserRoleEntity {
    private int userRoleId;
    private int userId;
    private int roleId;
    private String userRoleActiveFlag;
    private Timestamp userRoleCreateDate;
    private String userRoleCreateBy;
    private Timestamp userRoleUpdateDate;
    private String userRoleUpdateBy;

    @Id
    @Column(name = "user_role_id")
    public int getUserRoleId() {
        return userRoleId;
    }

    public void setUserRoleId(int userRoleId) {
        this.userRoleId = userRoleId;
    }

    @Basic
    @Column(name = "user_id")
    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    @Basic
    @Column(name = "role_id")
    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    @Basic
    @Column(name = "user_role_active_flag")
    public String getUserRoleActiveFlag() {
        return userRoleActiveFlag;
    }

    public void setUserRoleActiveFlag(String userRoleActiveFlag) {
        this.userRoleActiveFlag = userRoleActiveFlag;
    }

    @Basic
    @Column(name = "user_role_create_date")
    public Timestamp getUserRoleCreateDate() {
        return userRoleCreateDate;
    }

    public void setUserRoleCreateDate(Timestamp userRoleCreateDate) {
        this.userRoleCreateDate = userRoleCreateDate;
    }

    @Basic
    @Column(name = "user_role_create_by")
    public String getUserRoleCreateBy() {
        return userRoleCreateBy;
    }

    public void setUserRoleCreateBy(String userRoleCreateBy) {
        this.userRoleCreateBy = userRoleCreateBy;
    }

    @Basic
    @Column(name = "user_role_update_date")
    public Timestamp getUserRoleUpdateDate() {
        return userRoleUpdateDate;
    }

    public void setUserRoleUpdateDate(Timestamp userRoleUpdateDate) {
        this.userRoleUpdateDate = userRoleUpdateDate;
    }

    @Basic
    @Column(name = "user_role_update_by")
    public String getUserRoleUpdateBy() {
        return userRoleUpdateBy;
    }

    public void setUserRoleUpdateBy(String userRoleUpdateBy) {
        this.userRoleUpdateBy = userRoleUpdateBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserRoleEntity that = (UserRoleEntity) o;
        return userRoleId == that.userRoleId &&
                userId == that.userId &&
                roleId == that.roleId &&
                Objects.equals(userRoleActiveFlag, that.userRoleActiveFlag) &&
                Objects.equals(userRoleCreateDate, that.userRoleCreateDate) &&
                Objects.equals(userRoleCreateBy, that.userRoleCreateBy) &&
                Objects.equals(userRoleUpdateDate, that.userRoleUpdateDate) &&
                Objects.equals(userRoleUpdateBy, that.userRoleUpdateBy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(userRoleId, userId, roleId, userRoleActiveFlag, userRoleCreateDate, userRoleCreateBy, userRoleUpdateDate, userRoleUpdateBy);
    }
}

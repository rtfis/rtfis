package com.rtfis.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "role")
public class RoleEntity {
    private int roleId;
    private String roleName;
    private String roleDescription;
    private String roleActiveFlag;
    private Timestamp roleCreateDate;
    private String roleCreateBy;
    private Timestamp roleUpdateDate;
    private String roleUpdateBy;

    @Id
    @Column(name = "role_id")
    public int getRoleId() {
        return roleId;
    }

    public void setRoleId(int roleId) {
        this.roleId = roleId;
    }

    @Basic
    @Column(name = "role_name")
    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    @Basic
    @Column(name = "role_description")
    public String getRoleDescription() {
        return roleDescription;
    }

    public void setRoleDescription(String roleDescription) {
        this.roleDescription = roleDescription;
    }

    @Basic
    @Column(name = "role_active_flag")
    public String getRoleActiveFlag() {
        return roleActiveFlag;
    }

    public void setRoleActiveFlag(String roleActiveFlag) {
        this.roleActiveFlag = roleActiveFlag;
    }

    @Basic
    @Column(name = "role_create_date")
    public Timestamp getRoleCreateDate() {
        return roleCreateDate;
    }

    public void setRoleCreateDate(Timestamp roleCreateDate) {
        this.roleCreateDate = roleCreateDate;
    }

    @Basic
    @Column(name = "role_create_by")
    public String getRoleCreateBy() {
        return roleCreateBy;
    }

    public void setRoleCreateBy(String roleCreateBy) {
        this.roleCreateBy = roleCreateBy;
    }

    @Basic
    @Column(name = "role_update_date")
    public Timestamp getRoleUpdateDate() {
        return roleUpdateDate;
    }

    public void setRoleUpdateDate(Timestamp roleUpdateDate) {
        this.roleUpdateDate = roleUpdateDate;
    }

    @Basic
    @Column(name = "role_update_by")
    public String getRoleUpdateBy() {
        return roleUpdateBy;
    }

    public void setRoleUpdateBy(String roleUpdateBy) {
        this.roleUpdateBy = roleUpdateBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoleEntity that = (RoleEntity) o;
        return roleId == that.roleId &&
                Objects.equals(roleName, that.roleName) &&
                Objects.equals(roleDescription, that.roleDescription) &&
                Objects.equals(roleActiveFlag, that.roleActiveFlag) &&
                Objects.equals(roleCreateDate, that.roleCreateDate) &&
                Objects.equals(roleCreateBy, that.roleCreateBy) &&
                Objects.equals(roleUpdateDate, that.roleUpdateDate) &&
                Objects.equals(roleUpdateBy, that.roleUpdateBy);
    }

    @Override
    public int hashCode() {
        return Objects.hash(roleId, roleName, roleDescription, roleActiveFlag, roleCreateDate, roleCreateBy, roleUpdateDate, roleUpdateBy);
    }
}

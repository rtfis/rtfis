package com.rtfis.entities;

import javax.persistence.*;

@Entity
@Table(name = "ms_search_type")
public class MsSearchTypeEntity {
    @Id
    @Column(name = "search_type_id")
    private int searchTypeId;

    @Column(name = "search_type_group")
    private String searchTypeGroup;

    @Column(name = "search_type_name_th")
    private String searchTypeNameTh;

    @Column(name = "search_type_name_en")
    private String searchTypeNameEn;

    @Column(name = "search_type_note")
    private String searchTypeNote;

    public int getSearchTypeId() {
        return searchTypeId;
    }

    public void setSearchTypeId(int searchTypeId) {
        this.searchTypeId = searchTypeId;
    }

    public String getSearchTypeGroup() {
        return searchTypeGroup;
    }

    public void setSearchTypeGroup(String searchTypeGroup) {
        this.searchTypeGroup = searchTypeGroup;
    }

    public String getSearchTypeNameTh() {
        return searchTypeNameTh;
    }

    public void setSearchTypeNameTh(String searchTypeNameTh) {
        this.searchTypeNameTh = searchTypeNameTh;
    }

    public String getSearchTypeNameEn() {
        return searchTypeNameEn;
    }

    public void setSearchTypeNameEn(String searchTypeNameEn) {
        this.searchTypeNameEn = searchTypeNameEn;
    }

    public String getSearchTypeNote() {
        return searchTypeNote;
    }

    public void setSearchTypeNote(String searchTypeNote) {
        this.searchTypeNote = searchTypeNote;
    }

    @Override
    public String toString() {
        return "MsSearchTypeEntity{" +
                "searchTypeId=" + searchTypeId +
                ", searchTypeGroup='" + searchTypeGroup + '\'' +
                ", searchTypeNameTh='" + searchTypeNameTh + '\'' +
                ", searchTypeNameEn='" + searchTypeNameEn + '\'' +
                ", searchTypeNote='" + searchTypeNote + '\'' +
                '}';
    }
}

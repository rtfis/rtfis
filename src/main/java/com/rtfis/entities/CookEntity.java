package com.rtfis.entities;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "cook")
public class CookEntity implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "cook_id")
    private int cookId;
    @Column(name = "cook_formula_name")
    private String cookFormulaName;
    @Column(name = "cook_detail")
    private String cookDetail;
    @Column(name = "cook_create_date")
    private Timestamp cookCreateDate;
    @Column(name = "cook_create_by")
    private String cookCreateBy;
    @Column(name = "cook_update_date")
    private Timestamp cookUpdateDate;
    @Column(name = "cook_update_by")
    private String cookUpdateBy;

    @ManyToOne()
    @JoinColumn(name="dish_id", nullable=false)
    @JsonBackReference
    private DishEntity dish;

    @OneToMany(mappedBy = "cook",cascade = CascadeType.ALL,fetch = FetchType.LAZY,orphanRemoval = true)
    private List<CookIngredientEntity> cookIngredient = new ArrayList<>();

    public int getCookId() {
        return cookId;
    }

    public void setCookId(int cookId) {
        this.cookId = cookId;
    }

    public String getCookFormulaName() {
        return cookFormulaName;
    }

    public void setCookFormulaName(String cookFormulaName) {
        this.cookFormulaName = cookFormulaName;
    }

    public String getCookDetail() {
        return cookDetail;
    }

    public void setCookDetail(String cookDetail) {
        this.cookDetail = cookDetail;
    }

    public Timestamp getCookCreateDate() {
        return cookCreateDate;
    }

    public void setCookCreateDate(Timestamp cookCreateDate) {
        this.cookCreateDate = cookCreateDate;
    }

    public String getCookCreateBy() {
        return cookCreateBy;
    }

    public void setCookCreateBy(String cookCreateBy) {
        this.cookCreateBy = cookCreateBy;
    }

    public Timestamp getCookUpdateDate() {
        return cookUpdateDate;
    }

    public void setCookUpdateDate(Timestamp cookUpdateDate) {
        this.cookUpdateDate = cookUpdateDate;
    }

    public String getCookUpdateBy() {
        return cookUpdateBy;
    }

    public void setCookUpdateBy(String cookUpdateBy) {
        this.cookUpdateBy = cookUpdateBy;
    }

    public DishEntity getDish() {
        return dish;
    }

    public void setDish(DishEntity dish) {
        this.dish = dish;
    }

    public List<CookIngredientEntity> getCookIngredient() {
        return cookIngredient;
    }

    public void setCookIngredient(List<CookIngredientEntity> cookIngredient) {
        this.cookIngredient = cookIngredient;
    }
}

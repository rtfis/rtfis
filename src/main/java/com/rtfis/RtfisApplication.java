package com.rtfis;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RtfisApplication {

	public static void main(String[] args) {
		SpringApplication.run(RtfisApplication.class, args);
	}

}

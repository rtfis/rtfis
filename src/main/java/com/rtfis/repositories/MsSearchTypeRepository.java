package com.rtfis.repositories;

import com.rtfis.entities.MsSearchTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MsSearchTypeRepository extends JpaRepository<MsSearchTypeEntity, Integer> {
    @Query(value = "select * from ms_search_type where search_type_group = 1", nativeQuery = true)
    List<MsSearchTypeEntity> findGroup01();

    @Query(value = "select * from ms_search_type where search_type_group = 2", nativeQuery = true)
    List<MsSearchTypeEntity> findGroup02();
}

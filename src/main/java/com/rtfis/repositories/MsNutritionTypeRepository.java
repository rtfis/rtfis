package com.rtfis.repositories;

import com.rtfis.entities.MsNutritionTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MsNutritionTypeRepository extends JpaRepository<MsNutritionTypeEntity,Integer> {
}

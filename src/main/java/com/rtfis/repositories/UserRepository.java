package com.rtfis.repositories;

import com.rtfis.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity,Integer> {
    @Query(value = "select * from user where user_email =:userEmail and user_password =:userPassword",nativeQuery = true)
    UserEntity getUserByEmailAddPassword(@Param("userEmail") String userEmail,@Param("userPassword") String userPassword);
}

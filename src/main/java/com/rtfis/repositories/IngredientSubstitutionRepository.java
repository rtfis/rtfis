package com.rtfis.repositories;

import com.rtfis.entities.IngredientSubstitutionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IngredientSubstitutionRepository extends JpaRepository<IngredientSubstitutionEntity,Integer> {
}

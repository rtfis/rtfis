package com.rtfis.repositories;

import com.rtfis.entities.CookEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CookRepository extends JpaRepository<CookEntity,Integer> {
}

package com.rtfis.repositories;

import com.rtfis.entities.CookIngredientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CookIngredientRepository extends JpaRepository<CookIngredientEntity, Integer> {
}

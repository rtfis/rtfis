package com.rtfis.repositories;

import com.rtfis.entities.DishEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DishRepository extends JpaRepository<DishEntity, Integer> {
    List<DishEntity> findAllByOrderByDishIdDesc();

    @Query(value = "select * from dish where dish_approve_flag = 'N'", nativeQuery = true)
    List<DishEntity> findNonApprove();

    @Query(value = "select * from dish where dish_approve_flag = 'Y'", nativeQuery = true)
    List<DishEntity> findApproved();
}

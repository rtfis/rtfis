package com.rtfis.repositories;

import com.rtfis.entities.MsDishTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MsDishTypeRepository extends JpaRepository<MsDishTypeEntity,Integer> {
}

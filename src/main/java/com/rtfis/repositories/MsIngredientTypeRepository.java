package com.rtfis.repositories;

import com.rtfis.entities.MsIngredientTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MsIngredientTypeRepository extends JpaRepository<MsIngredientTypeEntity,Integer> {
}

package com.rtfis.repositories;

import com.rtfis.entities.MsUnitTypeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MsUnitTypeRepository extends JpaRepository<MsUnitTypeEntity, Integer> {
}

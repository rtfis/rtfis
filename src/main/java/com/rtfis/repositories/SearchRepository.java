package com.rtfis.repositories;

import com.rtfis.bean.dto.*;
import com.rtfis.entities.DishEntity;
import com.rtfis.utilities.ApplicationUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class SearchRepository {

    private static final Logger LOGGER = LoggerFactory.getLogger(SearchRepository.class);

    @PersistenceContext
    private EntityManager entityManager;

    public List<ResultDishSearchType1DTO> dishType1(String dish) {
        StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("search_dish_type1");
        storedProcedure.registerStoredProcedureParameter("dish", String.class, ParameterMode.IN);
        storedProcedure.setParameter("dish", dish);
        List<Object[]> resultList = (List<Object[]>) storedProcedure.getResultList();
        List<ResultDishSearchType1DTO> resultDishSearchType1DTOList = new ArrayList<>();
        for (Object[] array : resultList) {
            ResultDishSearchType1DTO resultDishSearchType1DTO = new ResultDishSearchType1DTO();
            resultDishSearchType1DTO.setDishId(ApplicationUtil.toString(array[0]));
            resultDishSearchType1DTO.setDishFileUrl(ApplicationUtil.toString(array[1]));
            resultDishSearchType1DTO.setDishNameTh(ApplicationUtil.toString(array[2]));
            resultDishSearchType1DTO.setDishNameEn(ApplicationUtil.toString(array[3]));
            resultDishSearchType1DTO.setDishDescriptionTh(ApplicationUtil.toString(array[4]));
            resultDishSearchType1DTO.setDishDescriptionEn(ApplicationUtil.toString(array[5]));
            resultDishSearchType1DTOList.add(resultDishSearchType1DTO);
        }
        return resultDishSearchType1DTOList;
    }

    public List<ResultDishSearchType2DTO> dishType2(String dish) {
        StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("search_dish_type2");
        storedProcedure.registerStoredProcedureParameter("dish", String.class, ParameterMode.IN);
        storedProcedure.setParameter("dish", dish);
        List<Object[]> resultList = (List<Object[]>) storedProcedure.getResultList();
        List<ResultDishSearchType2DTO> resultDishSearchType2DTOList = new ArrayList<>();
        for (Object[] array : resultList) {
            ResultDishSearchType2DTO resultDishSearchType2DTO = new ResultDishSearchType2DTO();
            resultDishSearchType2DTO.setDishId(ApplicationUtil.toString(array[0]));
            resultDishSearchType2DTO.setDishFileUrl(ApplicationUtil.toString(array[1]));
            resultDishSearchType2DTO.setDishNameTh(ApplicationUtil.toString(array[2]));
            resultDishSearchType2DTO.setDishNameEn(ApplicationUtil.toString(array[3]));
            resultDishSearchType2DTO.setDishDescriptionTh(ApplicationUtil.toString(array[4]));
            resultDishSearchType2DTO.setDishDescriptionEn(ApplicationUtil.toString(array[5]));
            resultDishSearchType2DTO.setCookFormulaName(ApplicationUtil.toString(array[6]));
            resultDishSearchType2DTO.setCookDetail(ApplicationUtil.toString(array[7]));
            resultDishSearchType2DTOList.add(resultDishSearchType2DTO);
        }
        return resultDishSearchType2DTOList;
    }

    public List<ResultDishSearchType3DTO> dishType3(String dish) {
        StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("search_dish_type3");
        storedProcedure.registerStoredProcedureParameter("dish", String.class, ParameterMode.IN);
        storedProcedure.setParameter("dish", dish);
        List<Object[]> resultList = (List<Object[]>) storedProcedure.getResultList();
        List<ResultDishSearchType3DTO> resultDishSearchType3DTOList = new ArrayList<>();
        for (Object[] array : resultList) {
            ResultDishSearchType3DTO resultDishSearchType3DTO = new ResultDishSearchType3DTO();
            resultDishSearchType3DTO.setDishId(ApplicationUtil.toString(array[0]));
            resultDishSearchType3DTO.setDishFileUrl(ApplicationUtil.toString(array[1]));
            resultDishSearchType3DTO.setDishNameTh(ApplicationUtil.toString(array[2]));
            resultDishSearchType3DTO.setDishNameEn(ApplicationUtil.toString(array[3]));
            resultDishSearchType3DTO.setDishDescriptionTh(ApplicationUtil.toString(array[4]));
            resultDishSearchType3DTO.setDishDescriptionEn(ApplicationUtil.toString(array[5]));
            resultDishSearchType3DTO.setCookFormulaName(ApplicationUtil.toString(array[6]));
            resultDishSearchType3DTO.setCookDetail(ApplicationUtil.toString(array[7]));
            resultDishSearchType3DTOList.add(resultDishSearchType3DTO);
        }
        return resultDishSearchType3DTOList;
    }

    public List<ResultDishSearchType4DTO> dishType4(String dish) {
        StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("search_dish_type4");
        storedProcedure.registerStoredProcedureParameter("dish", String.class, ParameterMode.IN);
        storedProcedure.setParameter("dish", dish);
        List<Object[]> resultList = (List<Object[]>) storedProcedure.getResultList();
        List<ResultDishSearchType4DTO> resultDishSearchType4DTOList = new ArrayList<>();
        for (Object[] array : resultList) {
            ResultDishSearchType4DTO resultDishSearchType4DTO = new ResultDishSearchType4DTO();
            resultDishSearchType4DTO.setDishId(ApplicationUtil.toString(array[0]));
            resultDishSearchType4DTO.setDishFileUrl(ApplicationUtil.toString(array[1]));
            resultDishSearchType4DTO.setDishNameTh(ApplicationUtil.toString(array[2]));
            resultDishSearchType4DTO.setDishNameEn(ApplicationUtil.toString(array[3]));
            resultDishSearchType4DTO.setDishDescriptionTh(ApplicationUtil.toString(array[4]));
            resultDishSearchType4DTO.setDishDescriptionEn(ApplicationUtil.toString(array[5]));
            resultDishSearchType4DTO.setCookFormulaName(ApplicationUtil.toString(array[6]));
            resultDishSearchType4DTO.setCookDetail(ApplicationUtil.toString(array[7]));
            resultDishSearchType4DTO.setDishTypeNameTh(ApplicationUtil.toString(array[8]));
            resultDishSearchType4DTO.setDishTypeNameEn(ApplicationUtil.toString(array[9]));
            resultDishSearchType4DTO.setNutritionFileUrl(ApplicationUtil.toString(array[10]));
            resultDishSearchType4DTO.setNutritionTypeNameTh(ApplicationUtil.toString(array[11]));
            resultDishSearchType4DTO.setNutritionTypeNameEn(ApplicationUtil.toString(array[12]));
            resultDishSearchType4DTOList.add(resultDishSearchType4DTO);
        }
        return resultDishSearchType4DTOList;
    }

    public List<ResultDishSearchType5DTO> dishType5(String dish) {
        StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("search_dish_type5");
        storedProcedure.registerStoredProcedureParameter("dish", String.class, ParameterMode.IN);
        storedProcedure.setParameter("dish", dish);
        List<Object[]> resultList = (List<Object[]>) storedProcedure.getResultList();
        List<ResultDishSearchType5DTO> resultDishSearchType5DTOList = new ArrayList<>();
        for (Object[] array : resultList) {
            ResultDishSearchType5DTO resultDishSearchType5DTO = new ResultDishSearchType5DTO();
            resultDishSearchType5DTO.setDishId(ApplicationUtil.toString(array[0]));
            resultDishSearchType5DTO.setDishFileUrl(ApplicationUtil.toString(array[1]));
            resultDishSearchType5DTO.setDishNameTh(ApplicationUtil.toString(array[2]));
            resultDishSearchType5DTO.setDishNameEn(ApplicationUtil.toString(array[3]));
            resultDishSearchType5DTO.setDishTypeNameTh(ApplicationUtil.toString(array[4]));
            resultDishSearchType5DTO.setDishTypeNameEn(ApplicationUtil.toString(array[5]));
            resultDishSearchType5DTO.setDishDescriptionTh(ApplicationUtil.toString(array[6]));
            resultDishSearchType5DTO.setDishDescriptionEn(ApplicationUtil.toString(array[7]));
            resultDishSearchType5DTO.setCookFormulaName(ApplicationUtil.toString(array[8]));
            resultDishSearchType5DTO.setCookDetail(ApplicationUtil.toString(array[9]));
            resultDishSearchType5DTO.setIngredientFileUrl(ApplicationUtil.toString(array[10]));
            resultDishSearchType5DTO.setIngredientNameTh(ApplicationUtil.toString(array[11]));
            resultDishSearchType5DTO.setIngredientNameEn(ApplicationUtil.toString(array[12]));
            resultDishSearchType5DTO.setIngredientTypeNameTh(ApplicationUtil.toString(array[13]));
            resultDishSearchType5DTO.setIngredientTypeNameEn(ApplicationUtil.toString(array[14]));
            resultDishSearchType5DTO.setIngredientDetailTh(ApplicationUtil.toString(array[15]));
            resultDishSearchType5DTO.setIngredientDetailEn(ApplicationUtil.toString(array[16]));
            resultDishSearchType5DTO.setCookIngredientAmount(ApplicationUtil.toString(array[17]));
            resultDishSearchType5DTO.setUnitNameTh(ApplicationUtil.toString(array[18]));
            resultDishSearchType5DTO.setUnitNameEn(ApplicationUtil.toString(array[19]));
            resultDishSearchType5DTO.setNutritionFileUrl(ApplicationUtil.toString(array[20]));
            resultDishSearchType5DTO.setNutritionTypeNameTh(ApplicationUtil.toString(array[21]));
            resultDishSearchType5DTOList.add(resultDishSearchType5DTO);
        }
        return resultDishSearchType5DTOList;
    }

    public List<ResultDishSearchType6DTO> dishType6(String dish) {
        StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("search_dish_type6");
        storedProcedure.registerStoredProcedureParameter("dish", String.class, ParameterMode.IN);
        storedProcedure.setParameter("dish", dish);
        List<Object[]> resultList = (List<Object[]>) storedProcedure.getResultList();
        List<ResultDishSearchType6DTO> resultDishSearchType6DTOList = new ArrayList<>();
        for (Object[] array : resultList) {
            ResultDishSearchType6DTO resultDishSearchType6DTO = new ResultDishSearchType6DTO();
            resultDishSearchType6DTO.setDishId(ApplicationUtil.toString(array[0]));
            resultDishSearchType6DTO.setDishFileUrl(ApplicationUtil.toString(array[1]));
            resultDishSearchType6DTO.setDishNameTh(ApplicationUtil.toString(array[2]));
            resultDishSearchType6DTO.setDishNameEn(ApplicationUtil.toString(array[3]));
            resultDishSearchType6DTO.setDishTypeNameTh(ApplicationUtil.toString(array[4]));
            resultDishSearchType6DTO.setDishTypeNameEn(ApplicationUtil.toString(array[5]));
            resultDishSearchType6DTO.setDishDescriptionTh(ApplicationUtil.toString(array[6]));
            resultDishSearchType6DTO.setDishDescriptionEn(ApplicationUtil.toString(array[7]));
            resultDishSearchType6DTO.setCookFormulaName(ApplicationUtil.toString(array[8]));
            resultDishSearchType6DTO.setCookDetail(ApplicationUtil.toString(array[9]));
            resultDishSearchType6DTO.setIngredientFileUrl(ApplicationUtil.toString(array[10]));
            resultDishSearchType6DTO.setIngredientNameTh(ApplicationUtil.toString(array[11]));
            resultDishSearchType6DTO.setIngredientNameEn(ApplicationUtil.toString(array[12]));
            resultDishSearchType6DTO.setIngredientTypeNameTh(ApplicationUtil.toString(array[13]));
            resultDishSearchType6DTO.setIngredientTypeNameEn(ApplicationUtil.toString(array[14]));
            resultDishSearchType6DTO.setIngredientDetailTh(ApplicationUtil.toString(array[15]));
            resultDishSearchType6DTO.setIngredientDetailEn(ApplicationUtil.toString(array[16]));
            resultDishSearchType6DTO.setCookIngredientAmount(ApplicationUtil.toString(array[17]));
            resultDishSearchType6DTO.setUnitNameTh(ApplicationUtil.toString(array[18]));
            resultDishSearchType6DTO.setUnitNameEn(ApplicationUtil.toString(array[19]));
            resultDishSearchType6DTO.setNutritionFileUrl(ApplicationUtil.toString(array[20]));
            resultDishSearchType6DTO.setNutritionTypeNameTh(ApplicationUtil.toString(array[21]));
            resultDishSearchType6DTO.setNutritionTypeNameEn(ApplicationUtil.toString(array[22]));
            resultDishSearchType6DTOList.add(resultDishSearchType6DTO);
        }
        return resultDishSearchType6DTOList;
    }

    public List<ResultDishSearchType7DTO> dishType7(String dish) {
        StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("search_dish_type7");
        storedProcedure.registerStoredProcedureParameter("dish", String.class, ParameterMode.IN);
        storedProcedure.setParameter("dish", dish);
        List<Object[]> resultList = (List<Object[]>) storedProcedure.getResultList();
        List<ResultDishSearchType7DTO> resultDishSearchType7DTOList = new ArrayList<>();
        for (Object[] array : resultList) {
            ResultDishSearchType7DTO resultDishSearchType7DTO = new ResultDishSearchType7DTO();
            resultDishSearchType7DTO.setDishId(ApplicationUtil.toString(array[0]));
            resultDishSearchType7DTO.setDishFileUrl(ApplicationUtil.toString(array[1]));
            resultDishSearchType7DTO.setDishNameTh(ApplicationUtil.toString(array[2]));
            resultDishSearchType7DTO.setDishNameEn(ApplicationUtil.toString(array[3]));
            resultDishSearchType7DTO.setDishTypeNameTh(ApplicationUtil.toString(array[4]));
            resultDishSearchType7DTO.setDishTypeNameEn(ApplicationUtil.toString(array[5]));
            resultDishSearchType7DTO.setCookFormulaName(ApplicationUtil.toString(array[6]));
            resultDishSearchType7DTO.setCookDetail(ApplicationUtil.toString(array[7]));
            resultDishSearchType7DTO.setMainFileIngredient(ApplicationUtil.toString(array[8]));
            resultDishSearchType7DTO.setMainIngredientTh(ApplicationUtil.toString(array[9]));
            resultDishSearchType7DTO.setMainIngredientEn(ApplicationUtil.toString(array[10]));
            resultDishSearchType7DTO.setCookIngredientAmount(ApplicationUtil.toString(array[11]));
            resultDishSearchType7DTO.setUnitNameTh(ApplicationUtil.toString(array[12]));
            resultDishSearchType7DTO.setUnitNameEn(ApplicationUtil.toString(array[13]));
            resultDishSearchType7DTO.setMainIngredientDetailTh(ApplicationUtil.toString(array[14]));
            resultDishSearchType7DTO.setMainIngredientDetailEn(ApplicationUtil.toString(array[15]));
            resultDishSearchType7DTO.setSubFileIngredient(ApplicationUtil.toString(array[16]));
            resultDishSearchType7DTO.setSubIngredientTh(ApplicationUtil.toString(array[17]));
            resultDishSearchType7DTO.setSubIngredientEn(ApplicationUtil.toString(array[18]));
            resultDishSearchType7DTO.setSubIngredientDetailTh(ApplicationUtil.toString(array[19]));
            resultDishSearchType7DTO.setSubIngredientDetailEn(ApplicationUtil.toString(array[20]));
            resultDishSearchType7DTOList.add(resultDishSearchType7DTO);
        }
        return resultDishSearchType7DTOList;
    }

    public List<ResultDishSearchType8DTO> dishType8(String dish) {
        StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("search_dish_type8");
        storedProcedure.registerStoredProcedureParameter("dish", String.class, ParameterMode.IN);
        storedProcedure.setParameter("dish", dish);
        List<Object[]> resultList = (List<Object[]>) storedProcedure.getResultList();
        List<ResultDishSearchType8DTO> resultDishSearchType8DTOList = new ArrayList<>();
        for (Object[] array : resultList) {
            ResultDishSearchType8DTO resultDishSearchType8DTO = new ResultDishSearchType8DTO();
            resultDishSearchType8DTO.setDishId(ApplicationUtil.toString(array[0]));
            resultDishSearchType8DTO.setDishFileUrl(ApplicationUtil.toString(array[1]));
            resultDishSearchType8DTO.setDishNameTh(ApplicationUtil.toString(array[2]));
            resultDishSearchType8DTO.setDishNameEn(ApplicationUtil.toString(array[3]));
            resultDishSearchType8DTO.setCookFormulaName(ApplicationUtil.toString(array[4]));
            resultDishSearchType8DTO.setCookDetail(ApplicationUtil.toString(array[5]));
            resultDishSearchType8DTO.setMainFileIngredient(ApplicationUtil.toString(array[6]));
            resultDishSearchType8DTO.setMainIngredientTh(ApplicationUtil.toString(array[7]));
            resultDishSearchType8DTO.setMainIngredientEn(ApplicationUtil.toString(array[8]));
            resultDishSearchType8DTO.setCookIngredientAmount(ApplicationUtil.toString(array[9]));
            resultDishSearchType8DTO.setUnitNameTh(ApplicationUtil.toString(array[10]));
            resultDishSearchType8DTO.setUnitNameEn(ApplicationUtil.toString(array[11]));
            resultDishSearchType8DTO.setMainIngredientDetailTh(ApplicationUtil.toString(array[12]));
            resultDishSearchType8DTO.setMainIngredientDetailEn(ApplicationUtil.toString(array[13]));
            resultDishSearchType8DTO.setSubFileIngredient(ApplicationUtil.toString(array[14]));
            resultDishSearchType8DTO.setSubIngredientTh(ApplicationUtil.toString(array[15]));
            resultDishSearchType8DTO.setSubIngredientEn(ApplicationUtil.toString(array[16]));
            resultDishSearchType8DTO.setSubIngredientDetailTh(ApplicationUtil.toString(array[17]));
            resultDishSearchType8DTO.setSubIngredientDetailEn(ApplicationUtil.toString(array[18]));
            resultDishSearchType8DTOList.add(resultDishSearchType8DTO);
        }
        return resultDishSearchType8DTOList;
    }

    public List<ResultIngredientSearchType1DTO> ingredientType1(String ingredientIds) {
        StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("search_ingredient_type1");
        storedProcedure.registerStoredProcedureParameter("ingredientIds", String.class, ParameterMode.IN);
        storedProcedure.setParameter("ingredientIds", ingredientIds);
        List<Object[]> resultList = (List<Object[]>) storedProcedure.getResultList();
        List<ResultIngredientSearchType1DTO> resultIngredientSearchType1DTOList = new ArrayList<>();
        for (Object[] array : resultList) {
            ResultIngredientSearchType1DTO resultIngredientSearchType1DTO = new ResultIngredientSearchType1DTO();
            resultIngredientSearchType1DTO.setIngredientId(ApplicationUtil.toString(array[0]));
            resultIngredientSearchType1DTO.setFileDownloadUrl(ApplicationUtil.toString(array[1]));
            resultIngredientSearchType1DTO.setIngredientNameTh(ApplicationUtil.toString(array[2]));
            resultIngredientSearchType1DTO.setIngredientNameEn(ApplicationUtil.toString(array[3]));
            resultIngredientSearchType1DTO.setIngredientDetailTh(ApplicationUtil.toString(array[4]));
            resultIngredientSearchType1DTO.setIngredientDetailEn(ApplicationUtil.toString(array[5]));
            resultIngredientSearchType1DTO.setNutritionTypeNameTh(ApplicationUtil.toString(array[6]));
            resultIngredientSearchType1DTO.setNutritionTypeNameEn(ApplicationUtil.toString(array[7]));
            resultIngredientSearchType1DTOList.add(resultIngredientSearchType1DTO);
        }
        return resultIngredientSearchType1DTOList;
    }

    public List<ResultIngredientSearchType2DTO> ingredientType2(String ingredientIds) {
        StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("search_ingredient_type2");
        storedProcedure.registerStoredProcedureParameter("ingredientIds", String.class, ParameterMode.IN);
        storedProcedure.setParameter("ingredientIds", ingredientIds);
        List<Object[]> resultList = (List<Object[]>) storedProcedure.getResultList();
        List<ResultIngredientSearchType2DTO> resultIngredientSearchType2DTOList = new ArrayList<>();
        for (Object[] array : resultList) {
            ResultIngredientSearchType2DTO resultIngredientSearchType2DTO = new ResultIngredientSearchType2DTO();
            resultIngredientSearchType2DTO.setMainFileIngredient(ApplicationUtil.toString(array[0]));
            resultIngredientSearchType2DTO.setMainIngredientTh(ApplicationUtil.toString(array[1]));
            resultIngredientSearchType2DTO.setMainIngredientEn(ApplicationUtil.toString(array[2]));
            resultIngredientSearchType2DTO.setMainIngredientDetailTh(ApplicationUtil.toString(array[3]));
            resultIngredientSearchType2DTO.setMainIngredientDetailEn(ApplicationUtil.toString(array[4]));
            resultIngredientSearchType2DTO.setSubFileIngredient(ApplicationUtil.toString(array[5]));
            resultIngredientSearchType2DTO.setSubIngredientTh(ApplicationUtil.toString(array[6]));
            resultIngredientSearchType2DTO.setSubIngredientEn(ApplicationUtil.toString(array[7]));
            resultIngredientSearchType2DTO.setSubIngredientDetailTh(ApplicationUtil.toString(array[8]));
            resultIngredientSearchType2DTO.setSubIngredientDetailEn(ApplicationUtil.toString(array[9]));
            resultIngredientSearchType2DTOList.add(resultIngredientSearchType2DTO);
        }
        return resultIngredientSearchType2DTOList;
    }
    public List<ResultIngredientSearchType3DTO> ingredientType3(String ingredientIds) {
        StoredProcedureQuery storedProcedure = entityManager.createStoredProcedureQuery("search_ingredient_type3");
        storedProcedure.registerStoredProcedureParameter("ingredientIds", String.class, ParameterMode.IN);
        storedProcedure.setParameter("ingredientIds", ingredientIds);
        List<Object[]> resultList = (List<Object[]>) storedProcedure.getResultList();
        List<ResultIngredientSearchType3DTO> resultIngredientSearchType3DTOList = new ArrayList<>();
        for (Object[] array : resultList) {
            ResultIngredientSearchType3DTO resultIngredientSearchType3DTO = new ResultIngredientSearchType3DTO();
            resultIngredientSearchType3DTO.setDishId(ApplicationUtil.toString(array[0]));
            resultIngredientSearchType3DTO.setFilePath01(ApplicationUtil.toString(array[1]));
            resultIngredientSearchType3DTO.setDishNameTh(ApplicationUtil.toString(array[2]));
            resultIngredientSearchType3DTO.setDishNameEn(ApplicationUtil.toString(array[3]));
            resultIngredientSearchType3DTO.setDishTypeNameTh(ApplicationUtil.toString(array[4]));
            resultIngredientSearchType3DTO.setDishTypeNameEn(ApplicationUtil.toString(array[5]));
            resultIngredientSearchType3DTO.setDishDescriptionTh(ApplicationUtil.toString(array[6]));
            resultIngredientSearchType3DTO.setDishDescriptionEn(ApplicationUtil.toString(array[7]));
            resultIngredientSearchType3DTO.setCookFormulaName(ApplicationUtil.toString(array[8]));
            resultIngredientSearchType3DTO.setCookDetail(ApplicationUtil.toString(array[9]));
            resultIngredientSearchType3DTO.setFilePath02(ApplicationUtil.toString(array[10]));
            resultIngredientSearchType3DTO.setIngredientNameTh(ApplicationUtil.toString(array[11]));
            resultIngredientSearchType3DTO.setIngredientNameEn(ApplicationUtil.toString(array[12]));
            resultIngredientSearchType3DTO.setIngredientTypeNameTh(ApplicationUtil.toString(array[13]));
            resultIngredientSearchType3DTO.setIngredientTypeNameEn(ApplicationUtil.toString(array[14]));
            resultIngredientSearchType3DTO.setIngredientDetailTh(ApplicationUtil.toString(array[15]));
            resultIngredientSearchType3DTO.setIngredientDetailEn(ApplicationUtil.toString(array[16]));
            resultIngredientSearchType3DTO.setCookIngredientAmount(ApplicationUtil.toString(array[17]));
            resultIngredientSearchType3DTO.setUnitNameTh(ApplicationUtil.toString(array[18]));
            resultIngredientSearchType3DTO.setUnitNameEn(ApplicationUtil.toString(array[19]));
            resultIngredientSearchType3DTO.setFilePath03(ApplicationUtil.toString(array[20]));
            resultIngredientSearchType3DTO.setNutritionTypeNameTh(ApplicationUtil.toString(array[21]));
            resultIngredientSearchType3DTO.setNutritionTypeNameEn(ApplicationUtil.toString(array[22]));
            resultIngredientSearchType3DTOList.add(resultIngredientSearchType3DTO);
        }
        return resultIngredientSearchType3DTOList;
    }
}

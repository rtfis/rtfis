package com.rtfis.repositories;

import com.rtfis.entities.IngredientEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface IngredientRepository extends JpaRepository<IngredientEntity, Integer> {
    @Query(value = "select ingredient_type_name_en,ingredient_type_name_th,ingredient_id,ingredient_name_en,ingredient_name_th from ingredient left join ms_ingredient_type on ingredient.ingredient_type_id = ms_ingredient_type.ingredient_type_id order by ingredient_type_name_en", nativeQuery = true)
    List<Object[]> findBySearchGroup();
    List<IngredientEntity> findAllByOrderByIngredientIdDesc();
}

package com.rtfis.repositories;

import com.rtfis.entities.NutritionEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface NutritionRepository extends JpaRepository<NutritionEntity, Integer> {
    @Query(value = "select * from nutrition where nutrition_id in :ids",nativeQuery = true)
    Set<NutritionEntity> findByIds(@Param("ids") Integer[] nutritionIdList);
    List<NutritionEntity> findAllByOrderByNutritionIdDesc();
}

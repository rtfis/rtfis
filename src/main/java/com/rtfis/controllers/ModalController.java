package com.rtfis.controllers;

import com.rtfis.entities.*;
import com.rtfis.repositories.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@Controller
@ApiIgnore
public class ModalController {

    private final DishRepository dishRepository;
    private final NutritionRepository nutritionRepository;
    private final IngredientRepository ingredientRepository;
    private final MsDishTypeRepository dishTypeRepository;
    private final MsNutritionTypeRepository nutritionTypeRepository;
    private final MsUnitTypeRepository unitTypeRepository;
    private final MsIngredientTypeRepository ingredientTypeRepository;

    @Autowired
    public ModalController(DishRepository dishRepository, NutritionRepository nutritionRepository, IngredientRepository ingredientRepository, MsDishTypeRepository dishTypeRepository, MsNutritionTypeRepository nutritionTypeRepository, MsUnitTypeRepository unitTypeRepository, MsIngredientTypeRepository ingredientTypeRepository) {
        this.dishRepository = dishRepository;
        this.nutritionRepository = nutritionRepository;
        this.ingredientRepository = ingredientRepository;
        this.dishTypeRepository = dishTypeRepository;
        this.nutritionTypeRepository = nutritionTypeRepository;
        this.unitTypeRepository = unitTypeRepository;
        this.ingredientTypeRepository = ingredientTypeRepository;
    }


    // SEARCH
    @GetMapping("modal-search")
    public String search(ModelMap model) throws Exception {
        return "universal-search-result";
    }

    // REGISTER
    @GetMapping("modal-register")
    public String register(ModelMap model) throws Exception {
        return "components/register";
    }

    // LOGIN
    @GetMapping("modal-login")
    public String login(ModelMap model) throws Exception {
        return "components/login";
    }

    // THAI-DISH
    @GetMapping("modal-thai-dish-view/{id}")
    public String showModalViewThaiDish(@PathVariable("id") int id, Model model) throws Exception {
        DishEntity dishEntity = dishRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        model.addAttribute("dish",dishEntity);
        return "pages/crud/thai-dish/thai-dish-view";
    }

    @GetMapping("modal-thai-dish-add")
    public String thaiDishAdd(ModelMap model) throws Exception {
        return "pages/crud/thai-dish/thai-dish-add";
    }

    @GetMapping("modal-thai-dish-edit/{id}")
    public String showModalEditThaiDish(@PathVariable("id") int id, Model model) throws Exception {
        DishEntity dishEntity = dishRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        List<MsDishTypeEntity> dishTypeList = dishTypeRepository.findAll();
        List<MsUnitTypeEntity> unitTypeList = unitTypeRepository.findAll();
        model.addAttribute("dish",dishEntity);
        model.addAttribute("dishType",dishTypeList);
        model.addAttribute("unitType",unitTypeList);
        return "pages/crud/thai-dish/thai-dish-edit";
    }

    @GetMapping("reload-thaidish")
    public String reloadThaiDish(ModelMap model) throws Exception {
        model.addAttribute("dishList", dishRepository.findAllByOrderByDishIdDesc());
        return "pages/admin/admin-dish-maintain :: #thai-dish-maintain-content";
    }

    // NUTRITION
    @GetMapping("modal-nutrition-add")
    public String nutritionAdd(ModelMap model) throws Exception {
        return "pages/crud/nutrition/nutrition-add";
    }
    @GetMapping("modal-nutrition-edit/{id}")
    public String showModalEditNutrition(@PathVariable("id") int id, Model model) throws Exception {
        NutritionEntity nutritionEntity = nutritionRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        List<MsNutritionTypeEntity> nutritionTypeEntityList = nutritionTypeRepository.findAll();
        model.addAttribute("nutrition",nutritionEntity);
        model.addAttribute("nutritionType",nutritionTypeEntityList);
        return "pages/crud/nutrition/nutrition-edit";
    }
    @GetMapping("reload-nutrition")
    public String reloadNutrition(ModelMap model) throws Exception {
        model.addAttribute("nutritionList", nutritionRepository.findAllByOrderByNutritionIdDesc());
        return "pages/main-features/nutrition :: #nutrition-content";
    }

    // INGREDIENT
    @GetMapping("modal-ingredient-view/{id}")
    public String showModalViewIngredient(@PathVariable("id") int id, Model model) throws Exception {
        IngredientEntity ingredientEntity = ingredientRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        model.addAttribute("ingredient",ingredientEntity);
        return "pages/crud/ingredient/ingredient-view";
    }

    @GetMapping("modal-ingredient-add")
    public String ingredientAdd(ModelMap model) throws Exception {
        model.addAttribute("nutritionList", nutritionRepository.findAllByOrderByNutritionIdDesc());
        return "pages/crud/ingredient/ingredient-add";
    }

    @GetMapping("modal-ingredient-edit/{id}")
    public String showModalEditIngredient(@PathVariable("id") int id, Model model) throws Exception {
        IngredientEntity ingredientEntity = ingredientRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("Invalid user Id:" + id));
        List<NutritionEntity> nutritionList = nutritionRepository.findAll();
        List<MsIngredientTypeEntity> ingredientTypeList = ingredientTypeRepository.findAll();
        model.addAttribute("ingredient",ingredientEntity);
        model.addAttribute("nutritionList",nutritionList);
        model.addAttribute("ingredientTypeList",ingredientTypeList);
        return "pages/crud/ingredient/ingredient-edit";
    }

    @GetMapping("reload-ingredient")
    public String reloadIngredient(ModelMap model) throws Exception {
        model.addAttribute("ingredientList", ingredientRepository.findAllByOrderByIngredientIdDesc());
        return "pages/main-features/ingredient :: #ingredient-content";
    }
}

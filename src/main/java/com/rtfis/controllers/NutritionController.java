package com.rtfis.controllers;

import com.rtfis.bean.dto.CreateNutritionDTO;
import com.rtfis.bean.dto.UpdateDishDTO;
import com.rtfis.bean.dto.UpdateNutritionDTO;
import com.rtfis.entities.DishEntity;
import com.rtfis.entities.NutritionEntity;
import com.rtfis.services.NutritionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
public class NutritionController {
    private static final Logger LOGGER = LoggerFactory.getLogger(NutritionController.class);

    @Autowired
    NutritionService nutritionService;

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping(path = "nutrition/all")
    public List<NutritionEntity> findAll() throws Exception {
        List<NutritionEntity> nutritionEntityList = new ArrayList<>();
        try {
            nutritionEntityList = nutritionService.findAll();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return nutritionEntityList;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping(path = "nutrition/{id}")
    public NutritionEntity findById(@PathVariable int id) throws Exception {
        return nutritionService.findById(id);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping(path = "nutrition/add")
    public ResponseEntity<String> create(@Valid CreateNutritionDTO nutrition, BindingResult result) {
        try {
            if (result.hasErrors()) {
                return new ResponseEntity<String>("ERROR", HttpStatus.BAD_REQUEST);
            }
            nutritionService.create(nutrition);
            return new ResponseEntity<String>("SUCCESS", HttpStatus.CREATED);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return new ResponseEntity<String>("ERROR", HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PutMapping("nutrition/update")
    public ResponseEntity<String> update(@Valid UpdateNutritionDTO nutrition, BindingResult result) {
        try {
            if (result.hasErrors()) {
                return new ResponseEntity<String>("ERROR", HttpStatus.BAD_REQUEST);
            }
            nutritionService.update(nutrition);
            return new ResponseEntity<String>("SUCCESS", HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return new ResponseEntity<String>("ERROR", HttpStatus.OK);
        }
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @DeleteMapping("nutrition/delete/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") int id) {
        try {
            NutritionEntity nutrition = nutritionService.findById(id);
            nutritionService.delete(nutrition);
            return new ResponseEntity<String>("SUCCESS", HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return new ResponseEntity<String>("ERROR", HttpStatus.OK);
        }
    }
}

package com.rtfis.controllers;

import com.rtfis.entities.*;
import com.rtfis.services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class MasterController {

    @Autowired
    private MsSearchTypeService searchTypeService;

    @Autowired
    private MsDishTypeService dishTypeService;

    @Autowired
    private MsIngredientTypeService ingredientTypeService;

    @Autowired
    private MsNutritionTypeService nutritionTypeService;

    @Autowired
    private MsUnitTypeService unitTypeService;

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping(path = "master/dish-search-type")
    public List<MsSearchTypeEntity> getDishSearchType() {
        return searchTypeService.findGroup01();
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping(path = "master/ingredient-search-type")
    public List<MsSearchTypeEntity> getIngredientSearchType() {
        return searchTypeService.findGroup02();
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping(path = "master/dish-type")
    public List<MsDishTypeEntity> getAllDishType() {
        return dishTypeService.findAll();
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping(path = "master/ingredient-type")
    public List<MsIngredientTypeEntity> getAllIngredientType() {
        return ingredientTypeService.findAll();
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping(path = "master/nutrition-type")
    public List<MsNutritionTypeEntity> getAllNutritionType() {
        return nutritionTypeService.findAll();
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping(path = "master/unit-type")
    public List<MsUnitTypeEntity> getAllUnit() {
        return unitTypeService.findAll();
    }
}

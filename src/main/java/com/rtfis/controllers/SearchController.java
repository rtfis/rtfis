package com.rtfis.controllers;

import com.rtfis.bean.dto.*;
import com.rtfis.services.SearchService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class SearchController {
    private static final Logger LOGGER = LoggerFactory.getLogger(SearchController.class);

    @Autowired
    SearchService searchService;

    @PostMapping(path = "search/by/dish")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public String searchDishRouter(@RequestParam(name = "keyword") String keyword, @RequestParam(name = "searchType") int searchType, ModelMap model) {
        try {
            LOGGER.info("SEARCH DISH KEYWORD : " + keyword + " | SEARCH TYPE : " + searchType);
            if (1 == searchType) {
                // SEARCH DISH
                List<ResultDishSearchType1DTO> resultDishSearchType1DTOList = searchService.dishType1(keyword);
                model.addAttribute("resultDishSearchType1DTOList", resultDishSearchType1DTOList);
                return "components/search-result/dish-type-1";
            } else if (2 == searchType) {
                // SEARCH DISH & COOK FORMULA
                List<ResultDishSearchType2DTO> resultDishSearchType2DTOList = searchService.dishType2(keyword);
                model.addAttribute("resultDishSearchType2DTOList", resultDishSearchType2DTOList);
                return "components/search-result/dish-type-2";
            } else if (3 == searchType) {
                // SEARCH DISH & DISH TYPE
                List<ResultDishSearchType3DTO> resultDishSearchType3DTOList = searchService.dishType3(keyword);
                model.addAttribute("resultDishSearchType3DTOList", resultDishSearchType3DTOList);
                return "components/search-result/dish-type-3";
            } else if (4 == searchType) {
                // SEARCH DISH & DISH TYPE & COOK FORMULA & NUTRITION
                List<ResultDishSearchType4DTO> resultDishSearchType4DTOList = searchService.dishType4(keyword);
                model.addAttribute("resultDishSearchType4DTOList", resultDishSearchType4DTOList);
                return "components/search-result/dish-type-4";
            } else if (5 == searchType) {
                // SEARCH DISH & DISH TYPE & NUTRITION & ALL INGREDIENT
                List<ResultDishSearchType5DTO> resultDishSearchType5DTOList = searchService.dishType5(keyword);
                model.addAttribute("resultDishSearchType5DTOList", resultDishSearchType5DTOList);
                return "components/search-result/dish-type-5";
            } else if (6 == searchType) {
                // SEARCH DISH MENU & DISH TYPE & NUTRITION TYPE & MAIN INGREDIENT
                List<ResultDishSearchType6DTO> resultDishSearchType6DTOList = searchService.dishType6(keyword);
                model.addAttribute("resultDishSearchType6DTOList", resultDishSearchType6DTOList);
                return "components/search-result/dish-type-6";
            } else if (7 == searchType) {
                // SEARCH DISH MENU & DISH TYPE & NUTRITION TYPE & INGREDIENT SUBSTITUTION
                List<ResultDishSearchType7DTO> resultDishSearchType7DTOList = searchService.dishType7(keyword);
                model.addAttribute("resultDishSearchType7DTOList", resultDishSearchType7DTOList);
                return "components/search-result/dish-type-7";
            } else if (8 == searchType) {
                // SEARCH DISH & ONLY SUBSTITUTION
                List<ResultDishSearchType8DTO> resultDishSearchType8DTOList = searchService.dishType8(keyword);
                model.addAttribute("resultDishSearchType8DTOList", resultDishSearchType8DTOList);
                return "components/search-result/dish-type-8";
            } else {
                return "components/search-result/error";
            }
        } catch (Exception e) {
            return "components/search-result/error";
        }
    }

    @PostMapping(path = "search/by/ingredient")
    @CrossOrigin(origins = "*", allowedHeaders = "*")
    public String searchIngredientRouter(@RequestParam(name = "ingredientIds") String ingredientIds, @RequestParam(name = "searchType") int searchType, ModelMap model) {
        try {
            LOGGER.info("SEARCH INGREDIENT KEYWORD : " + ingredientIds + " | SEARCH TYPE : " + searchType);
            if (9 == searchType) {
                // SEARCH INGREDIENT & NUTRITION
                List<ResultIngredientSearchType1DTO> resultIngredientSearchType1DTOList = searchService.ingredientType1(ingredientIds);
                model.addAttribute("resultIngredientSearchType1DTOList", resultIngredientSearchType1DTOList);
                return "components/search-result/ingredient-type-1";
            } else if (10 == searchType) {
                // SEARCH MAIN INGREDIENT & INGREDIENT SUBSTITUTION
                List<ResultIngredientSearchType2DTO> resultIngredientSearchType2DTOList = searchService.ingredientType2(ingredientIds);
                model.addAttribute("resultIngredientSearchType2DTOList", resultIngredientSearchType2DTOList);
                return "components/search-result/ingredient-type-2";
            } else if (11 == searchType) {
                // SEARCH DISH BY INPUT  MAIN INGREDIENT
                List<ResultIngredientSearchType3DTO> resultIngredientSearchType3DTOList = searchService.ingredientType3(ingredientIds);
                model.addAttribute("resultIngredientSearchType3DTOList", resultIngredientSearchType3DTOList);
                return "components/search-result/ingredient-type-3";
            } else {
                return "components/search-result/error";
            }
        } catch (Exception e) {
            return "components/search-result/error";
        }
    }
}
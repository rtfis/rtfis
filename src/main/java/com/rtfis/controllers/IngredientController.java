package com.rtfis.controllers;

import com.rtfis.bean.dto.CreateIngredientDTO;
import com.rtfis.bean.dto.UpdateIngredientDTO;
import com.rtfis.bean.dto.UpdateNutritionDTO;
import com.rtfis.entities.IngredientEntity;
import com.rtfis.services.IngredientService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
public class IngredientController {
    private static final Logger LOGGER = LoggerFactory.getLogger(IngredientController.class);

    @Autowired
    private IngredientService ingredientService;

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping(path = "ingredient/all")
    public List<IngredientEntity> findAll() throws Exception {
        List<IngredientEntity> ingredientList = new ArrayList<>();
        try {
            ingredientList = ingredientService.findAll();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return ingredientList;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping(path = "ingredient/add")
    public ResponseEntity<String> create(@Valid CreateIngredientDTO ingredient, BindingResult result) {
        LOGGER.info(ingredient.toString());
        try {
            if (result.hasErrors()) {
                return new ResponseEntity<String>("ERROR", HttpStatus.BAD_REQUEST);
            }
            ingredientService.create(ingredient);
            return new ResponseEntity<String>("SUCCESS", HttpStatus.CREATED);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return new ResponseEntity<String>("ERROR", HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping(path = "ingredient/search-group")
    public String mappingSearchGroup() throws Exception {
        return ingredientService.mappingSearchGroup();
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PutMapping("ingredient/update")
    public ResponseEntity<String> update(@Valid UpdateIngredientDTO ingredient, BindingResult result) {
        try {
            if (result.hasErrors()) {
                return new ResponseEntity<String>("ERROR", HttpStatus.BAD_REQUEST);
            }
            ingredientService.update(ingredient);
            return new ResponseEntity<String>("SUCCESS", HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return new ResponseEntity<String>("ERROR", HttpStatus.OK);
        }
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @DeleteMapping("ingredient/delete/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") int id) {
        try {
            IngredientEntity ingredient = ingredientService.findById(id);
            ingredientService.delete(ingredient);
            return new ResponseEntity<String>("SUCCESS", HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return new ResponseEntity<String>("ERROR", HttpStatus.OK);
        }
    }
}

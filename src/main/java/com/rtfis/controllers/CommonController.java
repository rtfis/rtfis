package com.rtfis.controllers;

import com.rtfis.services.NutritionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommonController {
    @Autowired
    private NutritionService nutritionService;

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping(path = "acknowledge")
    public String acknowledge(){
        return "SUCCESS";
    }

}

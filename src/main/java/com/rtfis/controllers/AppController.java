package com.rtfis.controllers;

import com.rtfis.entities.DishEntity;
import com.rtfis.entities.IngredientEntity;
import com.rtfis.entities.MsUnitTypeEntity;
import com.rtfis.entities.NutritionEntity;
import com.rtfis.services.DishService;
import com.rtfis.services.IngredientService;
import com.rtfis.services.MsUnitTypeService;
import com.rtfis.services.NutritionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@Controller
@ApiIgnore
public class AppController {

    @Autowired
    DishService dishService;
    @Autowired
    IngredientService ingredientService;
    @Autowired
    NutritionService nutritionService;
    @Autowired
    MsUnitTypeService msUnitTypeService;


    @GetMapping("/")
    public String index(ModelMap model) throws Exception {
        List<DishEntity> dishEntityList = dishService.findAll();
        List<NutritionEntity> nutritionList = nutritionService.findAll();
        List<IngredientEntity> ingredientList = ingredientService.findAll();
        List<MsUnitTypeEntity> msUnitList = msUnitTypeService.findAll();
        model.addAttribute("dishList", dishEntityList);
        model.addAttribute("ingredientList", ingredientList);
        model.addAttribute("nutritionList", nutritionList);
        model.addAttribute("unitList", msUnitList);
        return "index";
    }


    @RequestMapping("/admin-login")
    public String login() {
        return "/pages/admin/admin-login";
    }
}

package com.rtfis.controllers;

import com.rtfis.bean.dto.CreateDishDTO;
import com.rtfis.bean.dto.UpdateDishDTO;
import com.rtfis.entities.DishEntity;
import com.rtfis.services.DishService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
public class DishController {
    private static final Logger LOGGER = LoggerFactory.getLogger(DishController.class);

    @Autowired
    DishService dishService;

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping(path = "dish/all")
    public List<DishEntity> findAll() throws Exception {
        List<DishEntity> dishEntityList = new ArrayList<>();
        try {
            dishEntityList = dishService.findAll();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return dishEntityList;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping(path = "dish/approved")
    public List<DishEntity> findApproved() throws Exception {
        List<DishEntity> dishEntityList = new ArrayList<>();
        try {
            dishEntityList = dishService.findApproved();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return dishEntityList;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping(path = "dish/non-approve")
    public List<DishEntity> findNonApprove() throws Exception {
        List<DishEntity> dishEntityList = new ArrayList<>();
        try {
            dishEntityList = dishService.findNonApprove();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        return dishEntityList;
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping(path = "dish/{id}")
    public DishEntity findById(@PathVariable int id) throws Exception {
        return dishService.findById(id);
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping(path = "dish/add")
    public ResponseEntity<String> create(@Valid CreateDishDTO dish, BindingResult result) {
        try {
            if (result.hasErrors()) {
                return new ResponseEntity<String>("ERROR", HttpStatus.BAD_REQUEST);
            }
            dishService.create(dish);
            return new ResponseEntity<String>("SUCCESS", HttpStatus.CREATED);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return new ResponseEntity<String>("ERROR", HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PutMapping("dish/update")
    public ResponseEntity<String> update(@Valid UpdateDishDTO dish, BindingResult result) {
        try {
            if (result.hasErrors()) {
                return new ResponseEntity<String>("ERROR", HttpStatus.BAD_REQUEST);
            }
            dishService.update(dish);
            return new ResponseEntity<String>("SUCCESS", HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return new ResponseEntity<String>("ERROR", HttpStatus.OK);
        }
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @DeleteMapping("dish/delete/{id}")
    public ResponseEntity<String> delete(@PathVariable("id") int id) {
        try {
            DishEntity dish = dishService.findById(id);
            dishService.delete(dish);
            return new ResponseEntity<String>("SUCCESS", HttpStatus.OK);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return new ResponseEntity<String>("ERROR", HttpStatus.OK);
        }
    }
}

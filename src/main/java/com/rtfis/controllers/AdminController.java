package com.rtfis.controllers;

import com.rtfis.entities.DishEntity;
import com.rtfis.services.DishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    DishService dishService;

    @GetMapping("")
    @Secured("ROLE_ADMIN")
    public String admin(ModelMap model) throws Exception {
        List<DishEntity> dishForReviewList = dishService.findNonApprove();
        List<DishEntity> dishEntityList = dishService.findAll();
        model.addAttribute("dishForReviewList", dishForReviewList);
        model.addAttribute("dishList", dishEntityList);
        return "/pages/admin/admin-home";
    }
}

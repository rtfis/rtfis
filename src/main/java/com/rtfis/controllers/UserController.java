package com.rtfis.controllers;

import com.rtfis.bean.dto.CreateUserDTO;
import com.rtfis.bean.UserLogin;
import com.rtfis.entities.UserEntity;
import com.rtfis.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

@RestController
public class UserController {
    private final static Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @GetMapping(path = "user/all")
    public ResponseEntity<List<UserEntity>> userList() throws Exception {
        List<UserEntity> userEntityList = new ArrayList<>();
        try {
            userEntityList = userService.listAll();
            return new ResponseEntity<List<UserEntity>>(userEntityList, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<List<UserEntity>>(userEntityList, HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping(path = "user/register")
    public ResponseEntity<String> register(
            @RequestParam("userPic") MultipartFile userPic,
            @RequestParam("userFileGroup") String userFileGroup,
            @RequestParam("userEmail") String userEmail,
            @RequestParam("userName") String userName,
            @RequestParam("userLastName") String userLastName,
            @RequestParam("userPassword") String userPassword
    ) throws Exception {
        try {
            CreateUserDTO userDTO = new CreateUserDTO();
            userDTO.setUserPic(userPic);
            userDTO.setUserFileGroup(userFileGroup);
            userDTO.setUserEmail(userEmail);
            userDTO.setUserName(userName);
            userDTO.setUserLastName(userLastName);
            userDTO.setUserPassword(userPassword);
            userService.register(userDTO);
            return new ResponseEntity<String>("SUCCESS", HttpStatus.CREATED);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
            return new ResponseEntity<String>("ERROR", HttpStatus.BAD_REQUEST);
        }
    }

    @CrossOrigin(origins = "*", allowedHeaders = "*")
    @PostMapping(path = "user/login", consumes = "application/json")
    public String login(@RequestBody UserLogin user) throws Exception {
        UserEntity userLogin = new UserEntity();
        try {
            userLogin = userService.login(user.getUserEmail(), user.getUserPassword());
            if (userLogin.getUserName().equals(null)) {
                return "ERROR";
            } else {
                return "SUCCESS";
            }
        } catch (Exception e) {
            return "EXCEPTION";
        }
    }
}

package com.rtfis.services;

import com.rtfis.bean.dto.CreateDishDTO;
import com.rtfis.bean.dto.CreateDishIngredientSubstitutionDTO;
import com.rtfis.bean.dto.CreateDishIngredientsDTO;
import com.rtfis.bean.dto.UpdateDishDTO;
import com.rtfis.entities.*;
import com.rtfis.repositories.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

@Service
public class DishServiceImpl implements DishService {
    private static final Logger LOGGER = LoggerFactory.getLogger(DishServiceImpl.class);

    @Autowired
    FileStoragePathService fileStoragePathService;

    @Autowired
    DishRepository dishRepository;

    @Autowired
    FileRepository fileRepository;

    @Autowired
    MsDishTypeRepository msDishTypeRepository;

    @Autowired
    IngredientRepository ingredientRepository;

    @Autowired
    MsUnitTypeRepository msUnitTypeRepository;

    @Autowired
    CookIngredientRepository cookIngredientRepository;

    @Autowired
    CookRepository cookRepository;

    @Override
    public void create(CreateDishDTO createDishDTO) {
        try {
            FileEntity file = fileStoragePathService.storeFile(createDishDTO.getDishPic(), "System", createDishDTO.getFileGroup());
            MsDishTypeEntity msDishTypeEntity = msDishTypeRepository.getOne(createDishDTO.getDishTypeId());

            DishEntity dishEntity = new DishEntity();
            dishEntity.setDishNameTh(createDishDTO.getDishNameTh());
            dishEntity.setDishNameEn(createDishDTO.getDishNameEn());
            dishEntity.setDishDescriptionTh(createDishDTO.getDishDescriptionTh());
            dishEntity.setDishDescriptionEn(createDishDTO.getDishDescriptionEn());
            dishEntity.setDishType(msDishTypeEntity);
            dishEntity.setDishCreateBy("USERNAME");
            dishEntity.setDishCreateDate(new Date(System.currentTimeMillis()));
            dishEntity.setDishUpdateBy("USERNAME");
            dishEntity.setFile(file);
            dishEntity.setDishApproveFlag("N");
            // COOKS
            List<CookEntity> cookEntities = new ArrayList<>();
            CookEntity cookEntity = new CookEntity();
            cookEntity.setDish(dishEntity);
            cookEntity.setCookFormulaName(createDishDTO.getCookFormulaName());
            cookEntity.setCookDetail(createDishDTO.getCookDetail());
            cookEntity.setCookCreateBy("USERNAME");
            cookEntity.setCookUpdateBy("USERNAME");
            List<CookIngredientEntity> cookIngredientEntities = new ArrayList<>();
            for (CreateDishIngredientsDTO ingredients : createDishDTO.getIngredients()) {
                CookIngredientEntity cookIngredientEntity = new CookIngredientEntity();
                cookIngredientEntity.setCookIngredientAmount(ingredients.getAmount());
                cookIngredientEntity.setCook(cookEntity);
                cookIngredientEntity.setCookIngredientIsMainFlag(ingredients.getCookIngredientIsMainFlag());
                cookIngredientEntity.setIngredient(ingredientRepository.getOne(ingredients.getIngredientId()));
                cookIngredientEntity.setUnit(msUnitTypeRepository.getOne(ingredients.getUnitId()));
                List<IngredientSubstitutionEntity> ingredientSubstitutionEntityList = new ArrayList<>();
                if (null != ingredients.getSubstitutions() && (!ingredients.getSubstitutions().isEmpty())) {
                    for (CreateDishIngredientSubstitutionDTO createDishIngredientSubstitutions : ingredients.getSubstitutions()) {
                        IngredientSubstitutionEntity ingredientSubstitutionEntity = new IngredientSubstitutionEntity();
                        ingredientSubstitutionEntity.setSubIngredient(ingredientRepository.getOne(createDishIngredientSubstitutions.getIngredientId()));
                        ingredientSubstitutionEntity.setCookIngredient(cookIngredientEntity);
                        ingredientSubstitutionEntity.setCookIngredientAmount(createDishIngredientSubstitutions.getAmount());
                        ingredientSubstitutionEntity.setUnit(msUnitTypeRepository.getOne(createDishIngredientSubstitutions.getUnitId()));
                        ingredientSubstitutionEntity.setMainIngredient(ingredientRepository.getOne(ingredients.getIngredientId()));
                        ingredientSubstitutionEntityList.add(ingredientSubstitutionEntity);
                    }
                }
                cookIngredientEntity.setIngredientSubstitutions(ingredientSubstitutionEntityList);
                cookIngredientEntities.add(cookIngredientEntity);
            }
            cookEntity.setCookIngredient(cookIngredientEntities);
            cookEntities.add(cookEntity);
            dishEntity.setCooks(cookEntities);
            file.setDish(dishEntity);
            dishRepository.save(dishEntity);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<DishEntity> findAll() {
        return dishRepository.findAllByOrderByDishIdDesc();
    }

    @Override
    public List<DishEntity> findApproved() throws Exception {
        return dishRepository.findApproved();
    }

    @Override
    public List<DishEntity> findNonApprove() throws Exception {
        return dishRepository.findNonApprove();
    }

    @Override
    public DishEntity findById(int id) {
        return dishRepository.findById(id).get();
    }

    @Override
    public void update(UpdateDishDTO updateDishDTO) throws Exception {
        try {
            MsDishTypeEntity msDishTypeEntity = msDishTypeRepository.getOne(updateDishDTO.getDishTypeId());
            DishEntity dishEntity = findById(updateDishDTO.getDishId());
            dishEntity.setDishNameTh(updateDishDTO.getDishNameTh());
            dishEntity.setDishNameEn(updateDishDTO.getDishNameEn());
            dishEntity.setDishType(msDishTypeEntity);
            dishEntity.setDishDescriptionTh(updateDishDTO.getDishDescriptionTh());
            dishEntity.setDishDescriptionEn(updateDishDTO.getDishDescriptionEn());
            if (updateDishDTO.getDishPic().isEmpty()) {
                dishRepository.save(dishEntity);
            } else {
                FileEntity fileEntity = fileRepository.findById(updateDishDTO.getFileId()).get();
                if ((updateDishDTO.getDishPic().getName().equals(fileEntity.getFileName())) && (updateDishDTO.getDishPic().getSize() == fileEntity.getFileSize())) {
                    LOGGER.info("FILE IS SAME EXISTING");
                } else {
                    FileEntity file = fileStoragePathService.storeFile(updateDishDTO.getDishPic(), "System", updateDishDTO.getFileGroup());
                    dishEntity.setFile(file);
                    file.setDish(dishEntity);
                    fileRepository.save(file);
                }
            }
            CookEntity cookEntity = cookRepository.findById(updateDishDTO.getCookId()).get();
            cookEntity.setCookFormulaName(updateDishDTO.getCookFormulaName());
            cookEntity.setCookDetail(updateDishDTO.getCookDetail());
            cookEntity.setCookUpdateBy("ADMIN");
            cookEntity.setCookUpdateDate(new Timestamp(System.currentTimeMillis()));
            cookRepository.save(cookEntity);
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        } finally {

        }

    }

    @Override
    public void save(DishEntity dishEntity) {
        dishRepository.save(dishEntity);
    }

    @Override
    public void delete(DishEntity dishEntity) {
        try{
            dishRepository.delete(dishEntity);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}

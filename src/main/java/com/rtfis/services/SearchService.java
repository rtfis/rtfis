package com.rtfis.services;

import com.rtfis.bean.dto.*;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface SearchService {
    List<ResultDishSearchType1DTO> dishType1(String dish);

    List<ResultDishSearchType2DTO> dishType2(String dish);

    List<ResultDishSearchType3DTO> dishType3(String dish);

    List<ResultDishSearchType4DTO> dishType4(String dish);

    List<ResultDishSearchType5DTO> dishType5(String dish);

    List<ResultDishSearchType6DTO> dishType6(String dish);

    List<ResultDishSearchType7DTO> dishType7(String dish);

    List<ResultDishSearchType8DTO> dishType8(String dish);

    List<ResultIngredientSearchType1DTO> ingredientType1(String ingredientIds);

    List<ResultIngredientSearchType2DTO> ingredientType2(String ingredientIds);

    List<ResultIngredientSearchType3DTO> ingredientType3(String ingredientIds);
}

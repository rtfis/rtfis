package com.rtfis.services;

import com.rtfis.entities.FileEntity;
import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;


public interface FileStoragePathService {
    FileEntity storeFile(MultipartFile file , String userName, String fileGroup);
    Resource loadFileAsResource(String fileName);
}

package com.rtfis.services;

import com.rtfis.repositories.MsSearchTypeRepository;
import com.rtfis.entities.MsSearchTypeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MsSearchTypeServiceImpl implements MsSearchTypeService {

    @Autowired
    private MsSearchTypeRepository searchTypeDAO;

    @Override
    public List<MsSearchTypeEntity> findAll() {
        return searchTypeDAO.findAll();
    }

    @Override
    public List<MsSearchTypeEntity> findGroup01() {
        return searchTypeDAO.findGroup01();
    }

    @Override
    public List<MsSearchTypeEntity> findGroup02() {
        return searchTypeDAO.findGroup02();
    }
}

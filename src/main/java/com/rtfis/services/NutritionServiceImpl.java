package com.rtfis.services;

import com.rtfis.bean.dto.CreateNutritionDTO;
import com.rtfis.bean.dto.UpdateNutritionDTO;
import com.rtfis.repositories.FileRepository;
import com.rtfis.repositories.NutritionRepository;
import com.rtfis.entities.FileEntity;
import com.rtfis.entities.NutritionEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

@Service
public class NutritionServiceImpl implements NutritionService {
    private static final Logger LOGGER = LoggerFactory.getLogger(NutritionServiceImpl.class);

    @Autowired
    FileStoragePathService fileStoragePathService;

    @Autowired
    private NutritionRepository nutritionRepository;

    @Autowired
    FileRepository fileRepository;

    @Override
    public List<NutritionEntity> findAll() throws Exception {
        return nutritionRepository.findAllByOrderByNutritionIdDesc();
    }

    @Override
    public void create(CreateNutritionDTO createNutritionDTO) throws Exception {
        FileEntity file = fileStoragePathService.storeFile(createNutritionDTO.getNutritionPic(), "System", createNutritionDTO.getFileGroup());
        NutritionEntity nutritionEntity = new NutritionEntity();
        nutritionEntity.setNutritionNameTh(createNutritionDTO.getNutritionNameTh());
        nutritionEntity.setNutritionNameEn(createNutritionDTO.getNutritionNameEn());
        nutritionEntity.setNutritionDetailTh(createNutritionDTO.getNutritionDetailTh());
        nutritionEntity.setNutritionDetailEn(createNutritionDTO.getNutritionDetailEn());
        nutritionEntity.setNutritionTypeId(createNutritionDTO.getNutritionTypeId());
        nutritionEntity.setNutritionCreateBy("USERNAME");
        nutritionEntity.setNutritionCreateDate(new Date(System.currentTimeMillis()));
        nutritionEntity.setNutritionUpdateBy("USERNAME");
        nutritionEntity.setFile(file);
        file.setNutrition(nutritionEntity);
        fileRepository.save(file);
    }

    @Override
    public NutritionEntity findById(int id) throws Exception {
        return nutritionRepository.findById(id).get();
    }

    @Override
    public void update(UpdateNutritionDTO updateNutritionDTO) throws Exception {
        try {
            NutritionEntity nutritionEntity = findById(updateNutritionDTO.getNutritionId());
            nutritionEntity.setNutritionNameTh(updateNutritionDTO.getNutritionNameTh());
            nutritionEntity.setNutritionNameEn(updateNutritionDTO.getNutritionNameEn());
            nutritionEntity.setNutritionDetailTh(updateNutritionDTO.getNutritionDetailTh());
            nutritionEntity.setNutritionDetailEn(updateNutritionDTO.getNutritionDetailEn());
            nutritionEntity.setNutritionTypeId(updateNutritionDTO.getNutritionTypeId());

            if (updateNutritionDTO.getNutritionPic().isEmpty()) {
                nutritionRepository.save(nutritionEntity);
            } else {
                FileEntity fileEntity = fileRepository.findById(updateNutritionDTO.getFileId()).get();
                if ((updateNutritionDTO.getNutritionPic().getName().equals(fileEntity.getFileName())) && (updateNutritionDTO.getNutritionPic().getSize() == fileEntity.getFileSize())) {
                    LOGGER.info("FILE IS SAME EXISTING");
                } else {
                    FileEntity file = fileStoragePathService.storeFile(updateNutritionDTO.getNutritionPic(), "System", updateNutritionDTO.getFileGroup());
                    nutritionEntity.setFile(file);
                    file.setNutrition(nutritionEntity);
                    fileRepository.save(file);
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        } finally {

        }
    }

    @Override
    public void save(NutritionEntity nutritionEntity) throws Exception {
        nutritionRepository.save(nutritionEntity);
    }

    @Override
    public void delete(NutritionEntity nutritionEntity) throws Exception {
        nutritionRepository.delete(nutritionEntity);
    }
}

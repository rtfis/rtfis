package com.rtfis.services;

import com.rtfis.entities.MsIngredientTypeEntity;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface MsIngredientTypeService {
    List<MsIngredientTypeEntity> findAll();
}

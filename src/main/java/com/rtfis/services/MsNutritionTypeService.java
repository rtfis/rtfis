package com.rtfis.services;

import com.rtfis.entities.MsNutritionTypeEntity;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface MsNutritionTypeService {
    List<MsNutritionTypeEntity> findAll();
}

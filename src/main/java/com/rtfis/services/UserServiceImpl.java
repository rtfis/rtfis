package com.rtfis.services;

import com.rtfis.bean.dto.CreateUserDTO;
import com.rtfis.repositories.UserRepository;
import com.rtfis.entities.FileEntity;
import com.rtfis.entities.UserEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import java.sql.Timestamp;
import java.util.List;

@Service
@Transactional
public class UserServiceImpl implements UserService {
    @Autowired
    FileStoragePathService fileStorePathService;

    @Autowired
    UserRepository userRepository;

    @Override
    public List<UserEntity> listAll() {
        return userRepository.findAll();
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public void register(CreateUserDTO user) {
        try {
            FileEntity file = fileStorePathService.storeFile(user.getUserPic(), "system", "user");
            UserEntity userEntity = new UserEntity();
            userEntity.setFileId(file.getFileId());
            userEntity.setUserEmail(user.getUserEmail());
            userEntity.setUserName(user.getUserName());
            userEntity.setUserLastName(user.getUserLastName());
            userEntity.setUserPassword(user.getUserPassword());
            userEntity.setUserActivatedFlag("N");
            userEntity.setUserCreateBy("system");
            userEntity.setUserCreateDate(new Timestamp(System.currentTimeMillis()));
            userEntity.setUserUpdateBy("system");

            //TODO

            userRepository.save(userEntity);
        } catch (Exception e) {
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
        }

    }

    @Override
    public UserEntity login(String userEmail, String userPassword) {
        return userRepository.getUserByEmailAddPassword(userEmail, userPassword);
    }
}

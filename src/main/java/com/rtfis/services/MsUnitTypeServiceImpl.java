package com.rtfis.services;

import com.rtfis.entities.MsUnitTypeEntity;
import com.rtfis.repositories.MsUnitTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MsUnitTypeServiceImpl implements MsUnitTypeService {

    @Autowired
    MsUnitTypeRepository unitRepository;

    @Override
    public List<MsUnitTypeEntity> findAll() {
        return unitRepository.findAll();
    }
}

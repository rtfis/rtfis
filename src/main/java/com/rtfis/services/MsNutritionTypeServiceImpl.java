package com.rtfis.services;

import com.rtfis.repositories.MsNutritionTypeRepository;
import com.rtfis.entities.MsNutritionTypeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MsNutritionTypeServiceImpl implements MsNutritionTypeService{
    @Autowired
    MsNutritionTypeRepository nutritionTypeDAO;


    @Override
    public List<MsNutritionTypeEntity> findAll() {
        return nutritionTypeDAO.findAll();
    }
}

package com.rtfis.services;

import com.rtfis.configurations.FileStorageProperties;
import com.rtfis.entities.FileEntity;
import com.rtfis.exception.FileStorageException;
import com.rtfis.exception.MyFileNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.sql.Timestamp;

@Service
public class FileStoragePathServiceImpl implements FileStoragePathService {
    private final Path fileStorageLocation;

    @Autowired
    public FileStoragePathServiceImpl(FileStorageProperties fileStorageProperties) {
        this.fileStorageLocation = Paths.get(fileStorageProperties.getUploadDir())
                .toAbsolutePath().normalize();

        try {
            Files.createDirectories(this.fileStorageLocation);
        } catch (Exception ex) {
            throw new FileStorageException("Could not create the directory where the uploaded files will be stored.", ex);
        }
    }

    public FileEntity storeFile(MultipartFile file , String userName, String fileGroup) {
        String fileName = StringUtils.cleanPath(file.getOriginalFilename());
        try {
            if(fileName.contains("..")) {
                throw new FileStorageException("SORRY! FILENAME CONTAINS INVALID PATH SEQUENCE " + fileName);
            }
            String fileDownloadUri = new StringBuilder().append("/downloadFileFromPath/").append(fileName).toString();
            Path targetLocation = this.fileStorageLocation.resolve(fileName);
            Files.copy(file.getInputStream(), targetLocation, StandardCopyOption.REPLACE_EXISTING);
            FileEntity imageFile = new FileEntity(fileName,file.getContentType());
            imageFile.setFileSize(Math.toIntExact(file.getSize()));
            imageFile.setFilePath(targetLocation.toString());
            imageFile.setFileStoreType("PATH");
            imageFile.setFileDownloadUrl(fileDownloadUri);
            imageFile.setFileCreateBy(userName);
            imageFile.setFileCreateDate(new Timestamp(System.currentTimeMillis()));
            imageFile.setFileGroup(fileGroup);
            imageFile.setFileUpdateBy(userName);
            return imageFile;
        } catch (IOException ex) {
            throw new FileStorageException("COULD NOT STORE FILE " + fileName + ". PLEASE TRY AGAIN!", ex);
        }
    }

    public Resource loadFileAsResource(String fileName) {
        try {
            Path filePath = this.fileStorageLocation.resolve(fileName).normalize();
            Resource resource = new UrlResource(filePath.toUri());
            if(resource.exists()) {
                return resource;
            } else {
                throw new MyFileNotFoundException("FILE NOT FOUND " + fileName);
            }
        } catch (MalformedURLException ex) {
            throw new MyFileNotFoundException("FILE NOT FOUND " + fileName, ex);
        }
    }
}

package com.rtfis.services;

import com.rtfis.bean.dto.CreateUserDTO;
import com.rtfis.entities.UserEntity;

import java.util.List;

public interface UserService {
    List<UserEntity> listAll();
    void register(CreateUserDTO user);
    UserEntity login(String userEmail,String userPassword);
}

package com.rtfis.services;

import com.rtfis.entities.MsUnitTypeEntity;

import java.util.List;

public interface MsUnitTypeService {
    List<MsUnitTypeEntity> findAll();
}

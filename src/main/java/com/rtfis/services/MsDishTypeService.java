package com.rtfis.services;

import com.rtfis.entities.MsDishTypeEntity;

import java.util.List;

public interface MsDishTypeService {
    List<MsDishTypeEntity> findAll();
}

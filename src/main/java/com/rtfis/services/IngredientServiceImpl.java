package com.rtfis.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rtfis.bean.dto.CreateIngredientDTO;
import com.rtfis.bean.dto.SearchGroupIngredientDTO;
import com.rtfis.bean.dto.UpdateIngredientDTO;
import com.rtfis.bean.select2.Children;
import com.rtfis.bean.select2.Group;
import com.rtfis.entities.FileEntity;
import com.rtfis.entities.IngredientEntity;
import com.rtfis.entities.IngredientSubstitutionEntity;
import com.rtfis.entities.NutritionEntity;
import com.rtfis.repositories.FileRepository;
import com.rtfis.repositories.IngredientRepository;
import com.rtfis.repositories.IngredientSubstitutionRepository;
import com.rtfis.repositories.NutritionRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class IngredientServiceImpl implements IngredientService {
    private static final Logger LOGGER = LoggerFactory.getLogger(IngredientServiceImpl.class);
    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Autowired
    FileStoragePathService fileStoragePathService;

    @Autowired
    IngredientRepository ingredientRepository;

    @Autowired
    IngredientSubstitutionRepository ingredientSubstitutionRepository;

    @Autowired
    NutritionRepository nutritionRepository;

    @Autowired
    FileRepository fileRepository;

    @Override
    public void create(CreateIngredientDTO createIngredientDTO) throws Exception {
        try {
            FileEntity file = fileStoragePathService.storeFile(createIngredientDTO.getIngredientPic(), "System", createIngredientDTO.getFileGroup());
            IngredientEntity ingredientEntity = new IngredientEntity();
            List<IngredientSubstitutionEntity> ingredientSubstitutionEntities = new ArrayList<>();
            if (null != createIngredientDTO.getIngredientSubstitutionIdList() && createIngredientDTO.getIngredientSubstitutionIdList().length > 0) {
                for (int idx = 0; idx < createIngredientDTO.getIngredientSubstitutionIdList().length; idx++) {
                    IngredientSubstitutionEntity ingredientSubstitutionEntity = new IngredientSubstitutionEntity();
                    ingredientSubstitutionEntity.setMainIngredient(ingredientEntity);
                    ingredientSubstitutionEntity.setSubIngredient(ingredientRepository.getOne(createIngredientDTO.getIngredientSubstitutionIdList()[idx]));
                    ingredientSubstitutionEntities.add(ingredientSubstitutionEntity);
                }
            }
            ingredientEntity.setIngredientNameTh(createIngredientDTO.getIngredientNameTh());
            ingredientEntity.setIngredientNameEn(createIngredientDTO.getIngredientNameEn());
            ingredientEntity.setIngredientDetailTh(createIngredientDTO.getIngredientDetailTh());
            ingredientEntity.setIngredientDetailEn(createIngredientDTO.getIngredientDetailEn());
            ingredientEntity.setIngredientTypeId(createIngredientDTO.getIngredientTypeId());
            ingredientEntity.setIngredientSubstitutions(ingredientSubstitutionEntities);
            ingredientEntity.setIngredientCreateBy("USERNAME");
            ingredientEntity.setIngredientCreateDate(new Date(System.currentTimeMillis()));
            ingredientEntity.setIngredientUpdateBy("USERNAME");
            if (null != createIngredientDTO.getNutritionIdList() && createIngredientDTO.getNutritionIdList().length > 1) {
                Set<NutritionEntity> nutritionEntityList = nutritionRepository.findByIds(createIngredientDTO.getNutritionIdList());
                ingredientEntity.setNutritions(nutritionEntityList);
            }
            ingredientEntity.setFile(file);
            file.setIngredient(ingredientEntity);
            fileRepository.save(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public List<IngredientEntity> findAll() throws Exception {
        return ingredientRepository.findAllByOrderByIngredientIdDesc();
    }

    @Override
    public IngredientEntity findById(Integer id) throws Exception {
        return ingredientRepository.findById(id).get();
    }

    @Override
    public void update(UpdateIngredientDTO ingredientDTO) throws Exception {
        try {
            IngredientEntity ingredientEntity = findById(ingredientDTO.getIngredientId());
            ingredientEntity.setIngredientNameTh(ingredientDTO.getIngredientNameTh());
            ingredientEntity.setIngredientNameEn(ingredientDTO.getIngredientNameEn());
            ingredientEntity.setIngredientDetailTh(ingredientDTO.getIngredientDetailTh());
            ingredientEntity.setIngredientDetailEn(ingredientDTO.getIngredientDetailEn());
            ingredientEntity.setIngredientTypeId(ingredientDTO.getIngredientTypeId());

            if (ingredientDTO.getIngredientPic().isEmpty()) {
                ingredientRepository.save(ingredientEntity);
            } else {
                FileEntity fileEntity = fileRepository.findById(ingredientDTO.getFileId()).get();
                if ((ingredientDTO.getIngredientPic().getName().equals(fileEntity.getFileName())) && (ingredientDTO.getIngredientPic().getSize() == fileEntity.getFileSize())) {
                    LOGGER.info("FILE IS SAME EXISTING");
                } else {
                    FileEntity file = fileStoragePathService.storeFile(ingredientDTO.getIngredientPic(), "System", ingredientDTO.getFileGroup());
                    ingredientEntity.setFile(file);
                    file.setIngredient(ingredientEntity);
                    fileRepository.save(file);
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        } finally {

        }
    }

    @Override
    public void save(IngredientEntity ingredientEntity) throws Exception {
        ingredientRepository.save(ingredientEntity);
    }

    @Override
    public void delete(IngredientEntity ingredientEntity) throws Exception {
        ingredientRepository.delete(ingredientEntity);
    }

    @Override
    public String mappingSearchGroup() throws Exception {
        List<Group> groupList = new ArrayList<>();
        List<Object[]> searchGroup = ingredientRepository.findBySearchGroup();
        List<SearchGroupIngredientDTO> searchGroupIngredientDTOList = new ArrayList<>();
        if (searchGroup.size() > 0) {
            for (Object[] item : searchGroup) {
                SearchGroupIngredientDTO searchGroupIngredientDTO = new SearchGroupIngredientDTO();
                searchGroupIngredientDTO.setIngredientTypeNameEn(item[0].toString());
                searchGroupIngredientDTO.setIngredientTypeNameTh(item[1].toString());
                searchGroupIngredientDTO.setIngredientId(item[2].toString());
                searchGroupIngredientDTO.setIngredientNameEn(item[3].toString());
                searchGroupIngredientDTO.setIngredientNameTh(item[4].toString());
                searchGroupIngredientDTOList.add(searchGroupIngredientDTO);
            }
            Map<String, Map<Object, List<SearchGroupIngredientDTO>>> map = searchGroupIngredientDTOList.stream().collect(Collectors.groupingBy(SearchGroupIngredientDTO::getIngredientTypeNameTh, Collectors.groupingBy(SearchGroupIngredientDTO::getIngredientNameTh)));
            if (!map.isEmpty()) {
                for (Map.Entry<String, Map<Object, List<SearchGroupIngredientDTO>>> entry : map.entrySet()) {
                    Group group = new Group();
                    group.setText(entry.getKey());
                    List<Children> childrenList = new ArrayList<>();
                    for (Map.Entry<Object, List<SearchGroupIngredientDTO>> value : entry.getValue().entrySet()) {
                        Children children = new Children();
                        children.setId(value.getValue().get(0).getIngredientId());
                        children.setText(value.getValue().get(0).getIngredientNameTh());
                        childrenList.add(children);
                    }
                    group.setChildren(childrenList);
                    groupList.add(group);
                }
            }
        }
        return MAPPER.writeValueAsString(groupList);
    }
}

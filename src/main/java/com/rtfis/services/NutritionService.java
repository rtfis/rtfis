package com.rtfis.services;

import com.rtfis.bean.dto.CreateNutritionDTO;
import com.rtfis.bean.dto.UpdateNutritionDTO;
import com.rtfis.entities.NutritionEntity;

import java.util.List;

public interface NutritionService {
    List<NutritionEntity> findAll() throws Exception;
    void create(CreateNutritionDTO createNutritionDTO) throws Exception;
    NutritionEntity findById(int id) throws Exception;
    void update(UpdateNutritionDTO updateNutritionDTO) throws Exception;
    void save(NutritionEntity nutritionEntity) throws Exception;
    void delete(NutritionEntity nutritionEntity) throws Exception;
}

package com.rtfis.services;

import com.rtfis.bean.dto.*;
import com.rtfis.repositories.SearchRepository;
import com.rtfis.entities.DishEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SearchServiceImpl implements SearchService {

    @Autowired
    SearchRepository searchRepository;


    @Override
    public List<ResultDishSearchType1DTO> dishType1(String dish) {
        return searchRepository.dishType1(dish);
    }

    @Override
    public List<ResultDishSearchType2DTO> dishType2(String dish) {
        return searchRepository.dishType2(dish);
    }

    @Override
    public List<ResultDishSearchType3DTO> dishType3(String dish) {
        return searchRepository.dishType3(dish);
    }

    @Override
    public List<ResultDishSearchType4DTO> dishType4(String dish) {
        return searchRepository.dishType4(dish);
    }

    @Override
    public List<ResultDishSearchType5DTO> dishType5(String dish) {
        return searchRepository.dishType5(dish);
    }

    @Override
    public List<ResultDishSearchType6DTO> dishType6(String dish) {
        return searchRepository.dishType6(dish);
    }

    @Override
    public List<ResultDishSearchType7DTO> dishType7(String dish) {
        return searchRepository.dishType7(dish);
    }

    @Override
    public List<ResultDishSearchType8DTO> dishType8(String dish) {
        return searchRepository.dishType8(dish);
    }

    @Override
    public List<ResultIngredientSearchType1DTO> ingredientType1(String ingredientIds) {
        return searchRepository.ingredientType1(ingredientIds);
    }

    @Override
    public List<ResultIngredientSearchType2DTO> ingredientType2(String ingredientIds) {
        return searchRepository.ingredientType2(ingredientIds);
    }

    @Override
    public List<ResultIngredientSearchType3DTO> ingredientType3(String ingredientIds) {
        return searchRepository.ingredientType3(ingredientIds);
    }
}

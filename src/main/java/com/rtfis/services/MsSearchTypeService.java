package com.rtfis.services;

import com.rtfis.entities.MsSearchTypeEntity;

import java.util.List;

public interface MsSearchTypeService {
    List<MsSearchTypeEntity> findAll();
    List<MsSearchTypeEntity> findGroup01();
    List<MsSearchTypeEntity> findGroup02();
}

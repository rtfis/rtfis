package com.rtfis.services;

import com.rtfis.repositories.MsDishTypeRepository;
import com.rtfis.entities.MsDishTypeEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MsDishTypeServiceImpl implements MsDishTypeService {

    @Autowired
    MsDishTypeRepository dishTypeDAO;


    @Override
    public List<MsDishTypeEntity> findAll() {
        return dishTypeDAO.findAll();
    }
}

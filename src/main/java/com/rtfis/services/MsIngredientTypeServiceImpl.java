package com.rtfis.services;

import com.rtfis.repositories.MsIngredientTypeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.rtfis.entities.MsIngredientTypeEntity;

import java.util.List;

@Service
public class MsIngredientTypeServiceImpl implements MsIngredientTypeService {

    @Autowired
    MsIngredientTypeRepository msIngredientTypeRepository;

    @Override
    public List<MsIngredientTypeEntity> findAll() {
        return msIngredientTypeRepository.findAll();
    }
}

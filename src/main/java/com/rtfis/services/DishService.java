package com.rtfis.services;

import com.rtfis.bean.dto.CreateDishDTO;
import com.rtfis.bean.dto.UpdateDishDTO;
import com.rtfis.entities.DishEntity;

import java.util.List;

public interface DishService {
    void create(CreateDishDTO createDishDTO) throws Exception;
    List<DishEntity> findAll() throws Exception;
    List<DishEntity> findApproved() throws Exception;
    List<DishEntity> findNonApprove() throws Exception;
    DishEntity findById(int id) throws Exception;
    void update(UpdateDishDTO updateDishDTO) throws Exception;
    void save(DishEntity dishEntity) throws Exception;
    void delete(DishEntity dishEntity) throws Exception;
}

package com.rtfis.services;

import com.rtfis.bean.dto.CreateIngredientDTO;
import com.rtfis.bean.dto.UpdateIngredientDTO;
import com.rtfis.entities.IngredientEntity;

import javax.transaction.Transactional;
import java.util.List;

@Transactional
public interface IngredientService {
    void create(CreateIngredientDTO createIngredientDTO) throws Exception;
    List<IngredientEntity> findAll() throws Exception;
    IngredientEntity findById(Integer id) throws Exception;
    void update(UpdateIngredientDTO ingredientDTO) throws Exception;
    void save(IngredientEntity ingredientEntity) throws Exception;
    void delete(IngredientEntity ingredientEntity) throws Exception;
    String mappingSearchGroup() throws Exception;
}

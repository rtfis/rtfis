package com.rtfis.bean.select2;

import java.util.List;

public class Group {
    private String text;
    private List<Children> children;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<Children> getChildren() {
        return children;
    }

    public void setChildren(List<Children> children) {
        this.children = children;
    }
}

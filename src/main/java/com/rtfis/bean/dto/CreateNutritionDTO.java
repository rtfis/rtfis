package com.rtfis.bean.dto;

import org.springframework.web.multipart.MultipartFile;

import java.io.Serializable;

public class CreateNutritionDTO implements Serializable {
    private MultipartFile nutritionPic;
    private String fileGroup;
    private String nutritionNameTh;
    private String nutritionNameEn;
    private String nutritionDetailTh;
    private String nutritionDetailEn;
    private int nutritionTypeId;

    public MultipartFile getNutritionPic() {
        return nutritionPic;
    }

    public void setNutritionPic(MultipartFile nutritionPic) {
        this.nutritionPic = nutritionPic;
    }

    public String getFileGroup() {
        return fileGroup;
    }

    public void setFileGroup(String fileGroup) {
        this.fileGroup = fileGroup;
    }

    public String getNutritionNameTh() {
        return nutritionNameTh;
    }

    public void setNutritionNameTh(String nutritionNameTh) {
        this.nutritionNameTh = nutritionNameTh;
    }

    public String getNutritionNameEn() {
        return nutritionNameEn;
    }

    public void setNutritionNameEn(String nutritionNameEn) {
        this.nutritionNameEn = nutritionNameEn;
    }

    public String getNutritionDetailTh() {
        return nutritionDetailTh;
    }

    public void setNutritionDetailTh(String nutritionDetailTh) {
        this.nutritionDetailTh = nutritionDetailTh;
    }

    public String getNutritionDetailEn() {
        return nutritionDetailEn;
    }

    public void setNutritionDetailEn(String nutritionDetailEn) {
        this.nutritionDetailEn = nutritionDetailEn;
    }

    public int getNutritionTypeId() {
        return nutritionTypeId;
    }

    public void setNutritionTypeId(int nutritionTypeId) {
        this.nutritionTypeId = nutritionTypeId;
    }
}

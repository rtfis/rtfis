package com.rtfis.bean.dto;

import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public class CreateDishDTO {
    private MultipartFile dishPic;
    private String fileGroup;
    private String dishNameTh;
    private String dishNameEn;
    private String dishDescriptionTh;
    private String dishDescriptionEn;
    private int dishTypeId;
    private String cookFormulaName;
    private String cookDetail;
    private List<CreateDishIngredientsDTO> ingredients;

    public MultipartFile getDishPic() {
        return dishPic;
    }

    public void setDishPic(MultipartFile dishPic) {
        this.dishPic = dishPic;
    }

    public String getFileGroup() {
        return fileGroup;
    }

    public void setFileGroup(String fileGroup) {
        this.fileGroup = fileGroup;
    }

    public String getDishNameTh() {
        return dishNameTh;
    }

    public void setDishNameTh(String dishNameTh) {
        this.dishNameTh = dishNameTh;
    }

    public String getDishNameEn() {
        return dishNameEn;
    }

    public void setDishNameEn(String dishNameEn) {
        this.dishNameEn = dishNameEn;
    }

    public String getDishDescriptionTh() {
        return dishDescriptionTh;
    }

    public void setDishDescriptionTh(String dishDescriptionTh) {
        this.dishDescriptionTh = dishDescriptionTh;
    }

    public String getDishDescriptionEn() {
        return dishDescriptionEn;
    }

    public void setDishDescriptionEn(String dishDescriptionEn) {
        this.dishDescriptionEn = dishDescriptionEn;
    }

    public int getDishTypeId() {
        return dishTypeId;
    }

    public void setDishTypeId(int dishTypeId) {
        this.dishTypeId = dishTypeId;
    }

    public String getCookFormulaName() {
        return cookFormulaName;
    }

    public void setCookFormulaName(String cookFormulaName) {
        this.cookFormulaName = cookFormulaName;
    }

    public String getCookDetail() {
        return cookDetail;
    }

    public void setCookDetail(String cookDetail) {
        this.cookDetail = cookDetail;
    }

    public List<CreateDishIngredientsDTO> getIngredients() {
        return ingredients;
    }

    public void setIngredients(List<CreateDishIngredientsDTO> ingredients) {
        this.ingredients = ingredients;
    }
}


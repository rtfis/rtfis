package com.rtfis.bean.dto;

import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;

public class CreateIngredientDTO {
    private MultipartFile ingredientPic;
    private String fileGroup;
    private String ingredientNameTh;
    private String ingredientNameEn;
    private String ingredientDetailTh;
    private String ingredientDetailEn;
    private Integer ingredientTypeId;
    private Integer[] ingredientSubstitutionIdList;
    private Integer[] nutritionIdList;

    public MultipartFile getIngredientPic() {
        return ingredientPic;
    }

    public void setIngredientPic(MultipartFile ingredientPic) {
        this.ingredientPic = ingredientPic;
    }

    public String getFileGroup() {
        return fileGroup;
    }

    public void setFileGroup(String fileGroup) {
        this.fileGroup = fileGroup;
    }

    public String getIngredientNameTh() {
        return ingredientNameTh;
    }

    public void setIngredientNameTh(String ingredientNameTh) {
        this.ingredientNameTh = ingredientNameTh;
    }

    public String getIngredientNameEn() {
        return ingredientNameEn;
    }

    public void setIngredientNameEn(String ingredientNameEn) {
        this.ingredientNameEn = ingredientNameEn;
    }

    public String getIngredientDetailTh() {
        return ingredientDetailTh;
    }

    public void setIngredientDetailTh(String ingredientDetailTh) {
        this.ingredientDetailTh = ingredientDetailTh;
    }

    public String getIngredientDetailEn() {
        return ingredientDetailEn;
    }

    public void setIngredientDetailEn(String ingredientDetailEn) {
        this.ingredientDetailEn = ingredientDetailEn;
    }

    public Integer getIngredientTypeId() {
        return ingredientTypeId;
    }

    public void setIngredientTypeId(Integer ingredientTypeId) {
        this.ingredientTypeId = ingredientTypeId;
    }

    public Integer[] getIngredientSubstitutionIdList() {
        return ingredientSubstitutionIdList;
    }

    public void setIngredientSubstitutionIdList(Integer[] ingredientSubstitutionIdList) {
        this.ingredientSubstitutionIdList = ingredientSubstitutionIdList;
    }

    public Integer[] getNutritionIdList() {
        return nutritionIdList;
    }

    public void setNutritionIdList(Integer[] nutritionIdList) {
        this.nutritionIdList = nutritionIdList;
    }

    @Override
    public String toString() {
        return "CreateIngredientDTO{" +
                "ingredientPic=" + ingredientPic +
                ", fileGroup='" + fileGroup + '\'' +
                ", ingredientNameTh='" + ingredientNameTh + '\'' +
                ", ingredientNameEn='" + ingredientNameEn + '\'' +
                ", ingredientDetailTh='" + ingredientDetailTh + '\'' +
                ", ingredientDetailEn='" + ingredientDetailEn + '\'' +
                ", ingredientTypeId=" + ingredientTypeId +
                ", ingredientSubstitutionIdList=" + Arrays.toString(ingredientSubstitutionIdList) +
                ", nutritionIdList=" + Arrays.toString(nutritionIdList) +
                '}';
    }
}


package com.rtfis.bean.dto;

import org.springframework.web.multipart.MultipartFile;

public class CreateUserDTO {
    private String userEmail;
    private String userName;
    private String userLastName;
    private String userPassword;
    private String userFileGroup;
    private MultipartFile userPic;

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserPassword() {
        return userPassword;
    }

    public void setUserPassword(String userPassword) {
        this.userPassword = userPassword;
    }

    public String getUserFileGroup() {
        return userFileGroup;
    }

    public void setUserFileGroup(String userFileGroup) {
        this.userFileGroup = userFileGroup;
    }

    public MultipartFile getUserPic() {
        return userPic;
    }

    public void setUserPic(MultipartFile userPic) {
        this.userPic = userPic;
    }

    @Override
    public String toString() {
        return "CreateUserDTO{" +
                "userEmail='" + userEmail + '\'' +
                ", userName='" + userName + '\'' +
                ", userLastName='" + userLastName + '\'' +
                ", userPassword='" + userPassword + '\'' +
                ", userFileGroup='" + userFileGroup + '\'' +
                ", userPic=" + userPic +
                '}';
    }
}

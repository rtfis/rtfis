package com.rtfis.bean.dto;

import java.io.Serializable;

public class ResultIngredientSearchType1DTO implements Serializable {
    private String ingredientId;
    private String fileDownloadUrl;
    private String ingredientNameTh;
    private String ingredientNameEn;
    private String ingredientDetailTh;
    private String ingredientDetailEn;
    private String nutritionTypeNameTh;
    private String nutritionTypeNameEn;

    public String getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(String ingredientId) {
        this.ingredientId = ingredientId;
    }

    public String getFileDownloadUrl() {
        return fileDownloadUrl;
    }

    public void setFileDownloadUrl(String fileDownloadUrl) {
        this.fileDownloadUrl = fileDownloadUrl;
    }

    public String getIngredientNameTh() {
        return ingredientNameTh;
    }

    public void setIngredientNameTh(String ingredientNameTh) {
        this.ingredientNameTh = ingredientNameTh;
    }

    public String getIngredientNameEn() {
        return ingredientNameEn;
    }

    public void setIngredientNameEn(String ingredientNameEn) {
        this.ingredientNameEn = ingredientNameEn;
    }

    public String getIngredientDetailTh() {
        return ingredientDetailTh;
    }

    public void setIngredientDetailTh(String ingredientDetailTh) {
        this.ingredientDetailTh = ingredientDetailTh;
    }

    public String getIngredientDetailEn() {
        return ingredientDetailEn;
    }

    public void setIngredientDetailEn(String ingredientDetailEn) {
        this.ingredientDetailEn = ingredientDetailEn;
    }

    public String getNutritionTypeNameTh() {
        return nutritionTypeNameTh;
    }

    public void setNutritionTypeNameTh(String nutritionTypeNameTh) {
        this.nutritionTypeNameTh = nutritionTypeNameTh;
    }

    public String getNutritionTypeNameEn() {
        return nutritionTypeNameEn;
    }

    public void setNutritionTypeNameEn(String nutritionTypeNameEn) {
        this.nutritionTypeNameEn = nutritionTypeNameEn;
    }

    @Override
    public String toString() {
        return "ResultIngredientSearchType1DTO{" +
                "ingredientId='" + ingredientId + '\'' +
                ", fileDownloadUrl='" + fileDownloadUrl + '\'' +
                ", ingredientNameTh='" + ingredientNameTh + '\'' +
                ", ingredientNameEn='" + ingredientNameEn + '\'' +
                ", ingredientDetailTh='" + ingredientDetailTh + '\'' +
                ", ingredientDetailEn='" + ingredientDetailEn + '\'' +
                ", nutritionTypeNameTh='" + nutritionTypeNameTh + '\'' +
                ", nutritionTypeNameEn='" + nutritionTypeNameEn + '\'' +
                '}';
    }
}

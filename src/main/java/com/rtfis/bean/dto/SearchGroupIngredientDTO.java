package com.rtfis.bean.dto;

public class SearchGroupIngredientDTO {
    private String ingredientTypeNameEn;
    private String ingredientTypeNameTh;
    private String ingredientId;
    private String ingredientNameEn;
    private String ingredientNameTh;

    public String getIngredientTypeNameEn() {
        return ingredientTypeNameEn;
    }

    public void setIngredientTypeNameEn(String ingredientTypeNameEn) {
        this.ingredientTypeNameEn = ingredientTypeNameEn;
    }

    public String getIngredientTypeNameTh() {
        return ingredientTypeNameTh;
    }

    public void setIngredientTypeNameTh(String ingredientTypeNameTh) {
        this.ingredientTypeNameTh = ingredientTypeNameTh;
    }

    public String getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(String ingredientId) {
        this.ingredientId = ingredientId;
    }

    public String getIngredientNameEn() {
        return ingredientNameEn;
    }

    public void setIngredientNameEn(String ingredientNameEn) {
        this.ingredientNameEn = ingredientNameEn;
    }

    public String getIngredientNameTh() {
        return ingredientNameTh;
    }

    public void setIngredientNameTh(String ingredientNameTh) {
        this.ingredientNameTh = ingredientNameTh;
    }

    @Override
    public String toString() {
        return "SearchGroupIngredientDTO{" +
                "ingredientTypeNameEn='" + ingredientTypeNameEn + '\'' +
                ", ingredientTypeNameTh='" + ingredientTypeNameTh + '\'' +
                ", ingredientId='" + ingredientId + '\'' +
                ", ingredientNameEn='" + ingredientNameEn + '\'' +
                ", ingredientNameTh='" + ingredientNameTh + '\'' +
                '}';
    }
}

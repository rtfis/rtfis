package com.rtfis.bean.dto;

import java.io.Serializable;

public class ResultIngredientSearchType2DTO implements Serializable {
    private String mainFileIngredient;
    private String mainIngredientTh;
    private String mainIngredientEn;
    private String mainIngredientDetailTh;
    private String mainIngredientDetailEn;
    private String subFileIngredient;
    private String subIngredientTh;
    private String subIngredientEn;
    private String subIngredientDetailTh;
    private String subIngredientDetailEn;

    public String getMainFileIngredient() {
        return mainFileIngredient;
    }

    public void setMainFileIngredient(String mainFileIngredient) {
        this.mainFileIngredient = mainFileIngredient;
    }

    public String getMainIngredientTh() {
        return mainIngredientTh;
    }

    public void setMainIngredientTh(String mainIngredientTh) {
        this.mainIngredientTh = mainIngredientTh;
    }

    public String getMainIngredientEn() {
        return mainIngredientEn;
    }

    public void setMainIngredientEn(String mainIngredientEn) {
        this.mainIngredientEn = mainIngredientEn;
    }

    public String getMainIngredientDetailTh() {
        return mainIngredientDetailTh;
    }

    public void setMainIngredientDetailTh(String mainIngredientDetailTh) {
        this.mainIngredientDetailTh = mainIngredientDetailTh;
    }

    public String getMainIngredientDetailEn() {
        return mainIngredientDetailEn;
    }

    public void setMainIngredientDetailEn(String mainIngredientDetailEn) {
        this.mainIngredientDetailEn = mainIngredientDetailEn;
    }

    public String getSubFileIngredient() {
        return subFileIngredient;
    }

    public void setSubFileIngredient(String subFileIngredient) {
        this.subFileIngredient = subFileIngredient;
    }

    public String getSubIngredientTh() {
        return subIngredientTh;
    }

    public void setSubIngredientTh(String subIngredientTh) {
        this.subIngredientTh = subIngredientTh;
    }

    public String getSubIngredientEn() {
        return subIngredientEn;
    }

    public void setSubIngredientEn(String subIngredientEn) {
        this.subIngredientEn = subIngredientEn;
    }

    public String getSubIngredientDetailTh() {
        return subIngredientDetailTh;
    }

    public void setSubIngredientDetailTh(String subIngredientDetailTh) {
        this.subIngredientDetailTh = subIngredientDetailTh;
    }

    public String getSubIngredientDetailEn() {
        return subIngredientDetailEn;
    }

    public void setSubIngredientDetailEn(String subIngredientDetailEn) {
        this.subIngredientDetailEn = subIngredientDetailEn;
    }

    @Override
    public String toString() {
        return "ResultIngredientSearchType2DTO{" +
                "mainFileIngredient='" + mainFileIngredient + '\'' +
                ", mainIngredientTh='" + mainIngredientTh + '\'' +
                ", mainIngredientEn='" + mainIngredientEn + '\'' +
                ", mainIngredientDetailTh='" + mainIngredientDetailTh + '\'' +
                ", mainIngredientDetailEn='" + mainIngredientDetailEn + '\'' +
                ", subFileIngredient='" + subFileIngredient + '\'' +
                ", subIngredientTh='" + subIngredientTh + '\'' +
                ", subIngredientEn='" + subIngredientEn + '\'' +
                ", subIngredientDetailTh='" + subIngredientDetailTh + '\'' +
                ", subIngredientDetailEn='" + subIngredientDetailEn + '\'' +
                '}';
    }
}

package com.rtfis.bean.dto;

import java.io.Serializable;

public class ResultDishSearchType4DTO implements Serializable {
    private String dishId;
    private String dishFileUrl;
    private String dishNameTh;
    private String dishNameEn;
    private String dishDescriptionTh;
    private String dishDescriptionEn;
    private String cookFormulaName;
    private String cookDetail;
    private String dishTypeNameTh;
    private String dishTypeNameEn;
    private String nutritionFileUrl;
    private String nutritionTypeNameTh;
    private String nutritionTypeNameEn;

    public String getDishId() {
        return dishId;
    }

    public void setDishId(String dishId) {
        this.dishId = dishId;
    }

    public String getDishFileUrl() {
        return dishFileUrl;
    }

    public void setDishFileUrl(String dishFileUrl) {
        this.dishFileUrl = dishFileUrl;
    }

    public String getDishNameTh() {
        return dishNameTh;
    }

    public void setDishNameTh(String dishNameTh) {
        this.dishNameTh = dishNameTh;
    }

    public String getDishNameEn() {
        return dishNameEn;
    }

    public void setDishNameEn(String dishNameEn) {
        this.dishNameEn = dishNameEn;
    }

    public String getDishDescriptionTh() {
        return dishDescriptionTh;
    }

    public void setDishDescriptionTh(String dishDescriptionTh) {
        this.dishDescriptionTh = dishDescriptionTh;
    }

    public String getDishDescriptionEn() {
        return dishDescriptionEn;
    }

    public void setDishDescriptionEn(String dishDescriptionEn) {
        this.dishDescriptionEn = dishDescriptionEn;
    }

    public String getCookFormulaName() {
        return cookFormulaName;
    }

    public void setCookFormulaName(String cookFormulaName) {
        this.cookFormulaName = cookFormulaName;
    }

    public String getCookDetail() {
        return cookDetail;
    }

    public void setCookDetail(String cookDetail) {
        this.cookDetail = cookDetail;
    }

    public String getDishTypeNameTh() {
        return dishTypeNameTh;
    }

    public void setDishTypeNameTh(String dishTypeNameTh) {
        this.dishTypeNameTh = dishTypeNameTh;
    }

    public String getDishTypeNameEn() {
        return dishTypeNameEn;
    }

    public void setDishTypeNameEn(String dishTypeNameEn) {
        this.dishTypeNameEn = dishTypeNameEn;
    }

    public String getNutritionFileUrl() {
        return nutritionFileUrl;
    }

    public void setNutritionFileUrl(String nutritionFileUrl) {
        this.nutritionFileUrl = nutritionFileUrl;
    }

    public String getNutritionTypeNameTh() {
        return nutritionTypeNameTh;
    }

    public void setNutritionTypeNameTh(String nutritionTypeNameTh) {
        this.nutritionTypeNameTh = nutritionTypeNameTh;
    }

    public String getNutritionTypeNameEn() {
        return nutritionTypeNameEn;
    }

    public void setNutritionTypeNameEn(String nutritionTypeNameEn) {
        this.nutritionTypeNameEn = nutritionTypeNameEn;
    }

    @Override
    public String toString() {
        return "ResultDishSearchType4DTO{" +
                "dishId='" + dishId + '\'' +
                ", dishFileUrl='" + dishFileUrl + '\'' +
                ", dishNameTh='" + dishNameTh + '\'' +
                ", dishNameEn='" + dishNameEn + '\'' +
                ", dishDescriptionTh='" + dishDescriptionTh + '\'' +
                ", dishDescriptionEn='" + dishDescriptionEn + '\'' +
                ", cookFormulaName='" + cookFormulaName + '\'' +
                ", cookDetail='" + cookDetail + '\'' +
                ", dishTypeNameTh='" + dishTypeNameTh + '\'' +
                ", dishTypeNameEn='" + dishTypeNameEn + '\'' +
                ", nutritionFilePath='" + nutritionFileUrl + '\'' +
                ", nutritionTypeNameTh='" + nutritionTypeNameTh + '\'' +
                ", nutritionTypeNameEn='" + nutritionTypeNameEn + '\'' +
                '}';
    }
}


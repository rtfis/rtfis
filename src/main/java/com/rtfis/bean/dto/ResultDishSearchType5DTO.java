package com.rtfis.bean.dto;

import java.io.Serializable;

public class ResultDishSearchType5DTO implements Serializable {
    private String dishId;
    private String dishFileUrl;
    private String dishNameTh;
    private String dishNameEn;
    private String dishTypeNameTh;
    private String dishTypeNameEn;
    private String dishDescriptionTh;
    private String dishDescriptionEn;
    private String cookFormulaName;
    private String cookDetail;
    private String ingredientFileUrl;
    private String ingredientNameTh;
    private String ingredientNameEn;
    private String ingredientTypeNameTh;
    private String ingredientTypeNameEn;
    private String ingredientDetailTh;
    private String ingredientDetailEn;
    private String cookIngredientAmount;
    private String unitNameTh;
    private String unitNameEn;
    private String nutritionFileUrl;
    private String nutritionTypeNameTh;

    public String getDishId() {
        return dishId;
    }

    public void setDishId(String dishId) {
        this.dishId = dishId;
    }

    public String getDishFileUrl() {
        return dishFileUrl;
    }

    public void setDishFileUrl(String dishFileUrl) {
        this.dishFileUrl = dishFileUrl;
    }

    public String getDishNameTh() {
        return dishNameTh;
    }

    public void setDishNameTh(String dishNameTh) {
        this.dishNameTh = dishNameTh;
    }

    public String getDishNameEn() {
        return dishNameEn;
    }

    public void setDishNameEn(String dishNameEn) {
        this.dishNameEn = dishNameEn;
    }

    public String getDishTypeNameTh() {
        return dishTypeNameTh;
    }

    public void setDishTypeNameTh(String dishTypeNameTh) {
        this.dishTypeNameTh = dishTypeNameTh;
    }

    public String getDishTypeNameEn() {
        return dishTypeNameEn;
    }

    public void setDishTypeNameEn(String dishTypeNameEn) {
        this.dishTypeNameEn = dishTypeNameEn;
    }

    public String getDishDescriptionTh() {
        return dishDescriptionTh;
    }

    public void setDishDescriptionTh(String dishDescriptionTh) {
        this.dishDescriptionTh = dishDescriptionTh;
    }

    public String getDishDescriptionEn() {
        return dishDescriptionEn;
    }

    public void setDishDescriptionEn(String dishDescriptionEn) {
        this.dishDescriptionEn = dishDescriptionEn;
    }

    public String getCookFormulaName() {
        return cookFormulaName;
    }

    public void setCookFormulaName(String cookFormulaName) {
        this.cookFormulaName = cookFormulaName;
    }

    public String getCookDetail() {
        return cookDetail;
    }

    public void setCookDetail(String cookDetail) {
        this.cookDetail = cookDetail;
    }

    public String getIngredientFileUrl() {
        return ingredientFileUrl;
    }

    public void setIngredientFileUrl(String ingredientFileUrl) {
        this.ingredientFileUrl = ingredientFileUrl;
    }

    public String getIngredientNameTh() {
        return ingredientNameTh;
    }

    public void setIngredientNameTh(String ingredientNameTh) {
        this.ingredientNameTh = ingredientNameTh;
    }

    public String getIngredientNameEn() {
        return ingredientNameEn;
    }

    public void setIngredientNameEn(String ingredientNameEn) {
        this.ingredientNameEn = ingredientNameEn;
    }

    public String getIngredientTypeNameTh() {
        return ingredientTypeNameTh;
    }

    public void setIngredientTypeNameTh(String ingredientTypeNameTh) {
        this.ingredientTypeNameTh = ingredientTypeNameTh;
    }

    public String getIngredientTypeNameEn() {
        return ingredientTypeNameEn;
    }

    public void setIngredientTypeNameEn(String ingredientTypeNameEn) {
        this.ingredientTypeNameEn = ingredientTypeNameEn;
    }

    public String getIngredientDetailTh() {
        return ingredientDetailTh;
    }

    public void setIngredientDetailTh(String ingredientDetailTh) {
        this.ingredientDetailTh = ingredientDetailTh;
    }

    public String getIngredientDetailEn() {
        return ingredientDetailEn;
    }

    public void setIngredientDetailEn(String ingredientDetailEn) {
        this.ingredientDetailEn = ingredientDetailEn;
    }

    public String getCookIngredientAmount() {
        return cookIngredientAmount;
    }

    public void setCookIngredientAmount(String cookIngredientAmount) {
        this.cookIngredientAmount = cookIngredientAmount;
    }

    public String getUnitNameTh() {
        return unitNameTh;
    }

    public void setUnitNameTh(String unitNameTh) {
        this.unitNameTh = unitNameTh;
    }

    public String getUnitNameEn() {
        return unitNameEn;
    }

    public void setUnitNameEn(String unitNameEn) {
        this.unitNameEn = unitNameEn;
    }

    public String getNutritionFileUrl() {
        return nutritionFileUrl;
    }

    public void setNutritionFileUrl(String nutritionFileUrl) {
        this.nutritionFileUrl = nutritionFileUrl;
    }

    public String getNutritionTypeNameTh() {
        return nutritionTypeNameTh;
    }

    public void setNutritionTypeNameTh(String nutritionTypeNameTh) {
        this.nutritionTypeNameTh = nutritionTypeNameTh;
    }

    @Override
    public String toString() {
        return "ResultDishSearchType5DTO{" +
                "dishId='" + dishId + '\'' +
                ", dishFilePath='" + dishFileUrl + '\'' +
                ", dishNameTh='" + dishNameTh + '\'' +
                ", dishNameEn='" + dishNameEn + '\'' +
                ", dishTypeNameTh='" + dishTypeNameTh + '\'' +
                ", dishTypeNameEn='" + dishTypeNameEn + '\'' +
                ", dishDescriptionTh='" + dishDescriptionTh + '\'' +
                ", dishDescriptionEn='" + dishDescriptionEn + '\'' +
                ", cookFormulaName='" + cookFormulaName + '\'' +
                ", cookDetail='" + cookDetail + '\'' +
                ", ingredientFilePath='" + ingredientFileUrl + '\'' +
                ", ingredientNameTh='" + ingredientNameTh + '\'' +
                ", ingredientNameEn='" + ingredientNameEn + '\'' +
                ", ingredientTypeNameTh='" + ingredientTypeNameTh + '\'' +
                ", ingredientTypeNameEn='" + ingredientTypeNameEn + '\'' +
                ", ingredientDetailTh='" + ingredientDetailTh + '\'' +
                ", ingredientDetailEn='" + ingredientDetailEn + '\'' +
                ", cookIngredientAmount='" + cookIngredientAmount + '\'' +
                ", unitNameTh='" + unitNameTh + '\'' +
                ", unitNameEn='" + unitNameEn + '\'' +
                ", nutritionFilePath='" + nutritionFileUrl + '\'' +
                ", nutritionTypeNameTh='" + nutritionTypeNameTh + '\'' +
                '}';
    }
}

package com.rtfis.bean.dto;

public class CreateIngredientAddSubstitutionDTO {
    private int ingredientId;
    private String ingredientNameTh;
    private String ingredientNameEn;

    public int getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(int ingredientId) {
        this.ingredientId = ingredientId;
    }

    public String getIngredientNameTh() {
        return ingredientNameTh;
    }

    public void setIngredientNameTh(String ingredientNameTh) {
        this.ingredientNameTh = ingredientNameTh;
    }

    public String getIngredientNameEn() {
        return ingredientNameEn;
    }

    public void setIngredientNameEn(String ingredientNameEn) {
        this.ingredientNameEn = ingredientNameEn;
    }
}


package com.rtfis.bean.dto;

import org.springframework.web.multipart.MultipartFile;

public class UpdateDishDTO {
    private int dishId;
    private int fileId;
    private int cookId;
    private MultipartFile dishPic;
    private String fileGroup;
    private String dishNameTh;
    private String dishNameEn;
    private String dishDescriptionTh;
    private String dishDescriptionEn;
    private int dishTypeId;
    private String cookFormulaName;
    private String cookDetail;

    public int getDishId() {
        return dishId;
    }

    public void setDishId(int dishId) {
        this.dishId = dishId;
    }

    public int getFileId() {
        return fileId;
    }

    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    public int getCookId() {
        return cookId;
    }

    public void setCookId(int cookId) {
        this.cookId = cookId;
    }

    public MultipartFile getDishPic() {
        return dishPic;
    }

    public void setDishPic(MultipartFile dishPic) {
        this.dishPic = dishPic;
    }

    public String getFileGroup() {
        return fileGroup;
    }

    public void setFileGroup(String fileGroup) {
        this.fileGroup = fileGroup;
    }

    public String getDishNameTh() {
        return dishNameTh;
    }

    public void setDishNameTh(String dishNameTh) {
        this.dishNameTh = dishNameTh;
    }

    public String getDishNameEn() {
        return dishNameEn;
    }

    public void setDishNameEn(String dishNameEn) {
        this.dishNameEn = dishNameEn;
    }

    public String getDishDescriptionTh() {
        return dishDescriptionTh;
    }

    public void setDishDescriptionTh(String dishDescriptionTh) {
        this.dishDescriptionTh = dishDescriptionTh;
    }

    public String getDishDescriptionEn() {
        return dishDescriptionEn;
    }

    public void setDishDescriptionEn(String dishDescriptionEn) {
        this.dishDescriptionEn = dishDescriptionEn;
    }

    public int getDishTypeId() {
        return dishTypeId;
    }

    public void setDishTypeId(int dishTypeId) {
        this.dishTypeId = dishTypeId;
    }

    public String getCookFormulaName() {
        return cookFormulaName;
    }

    public void setCookFormulaName(String cookFormulaName) {
        this.cookFormulaName = cookFormulaName;
    }

    public String getCookDetail() {
        return cookDetail;
    }

    public void setCookDetail(String cookDetail) {
        this.cookDetail = cookDetail;
    }
}


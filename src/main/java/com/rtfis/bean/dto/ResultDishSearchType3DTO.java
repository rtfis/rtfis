package com.rtfis.bean.dto;

import java.io.Serializable;

public class ResultDishSearchType3DTO implements Serializable {
    private String dishId;
    private String dishFileUrl;
    private String dishNameTh;
    private String dishNameEn;
    private String dishDescriptionTh;
    private String dishDescriptionEn;
    private String cookFormulaName;
    private String cookDetail;

    public String getDishId() {
        return dishId;
    }

    public void setDishId(String dishId) {
        this.dishId = dishId;
    }

    public String getDishFileUrl() {
        return dishFileUrl;
    }

    public void setDishFileUrl(String dishFileUrl) {
        this.dishFileUrl = dishFileUrl;
    }

    public String getDishNameTh() {
        return dishNameTh;
    }

    public void setDishNameTh(String dishNameTh) {
        this.dishNameTh = dishNameTh;
    }

    public String getDishNameEn() {
        return dishNameEn;
    }

    public void setDishNameEn(String dishNameEn) {
        this.dishNameEn = dishNameEn;
    }

    public String getDishDescriptionTh() {
        return dishDescriptionTh;
    }

    public void setDishDescriptionTh(String dishDescriptionTh) {
        this.dishDescriptionTh = dishDescriptionTh;
    }

    public String getDishDescriptionEn() {
        return dishDescriptionEn;
    }

    public void setDishDescriptionEn(String dishDescriptionEn) {
        this.dishDescriptionEn = dishDescriptionEn;
    }

    public String getCookFormulaName() {
        return cookFormulaName;
    }

    public void setCookFormulaName(String cookFormulaName) {
        this.cookFormulaName = cookFormulaName;
    }

    public String getCookDetail() {
        return cookDetail;
    }

    public void setCookDetail(String cookDetail) {
        this.cookDetail = cookDetail;
    }

    @Override
    public String toString() {
        return "ResultDishSearchType3DTO{" +
                "dishId='" + dishId + '\'' +
                ", dishFileUrl='" + dishFileUrl + '\'' +
                ", dishNameTh='" + dishNameTh + '\'' +
                ", dishNameEn='" + dishNameEn + '\'' +
                ", dishDescriptionTh='" + dishDescriptionTh + '\'' +
                ", dishDescriptionEn='" + dishDescriptionEn + '\'' +
                ", cookFormulaName='" + cookFormulaName + '\'' +
                ", cookDetail='" + cookDetail + '\'' +
                '}';
    }
}

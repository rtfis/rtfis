package com.rtfis.bean.dto;

import java.util.List;

public class CreateDishIngredientsDTO {
    private Integer ingredientId;
    private String cookIngredientIsMainFlag;
    private String amount;
    private Integer unitId;
    private List<CreateDishIngredientSubstitutionDTO>  substitutions;

    public Integer getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(Integer ingredientId) {
        this.ingredientId = ingredientId;
    }

    public String getCookIngredientIsMainFlag() {
        return cookIngredientIsMainFlag;
    }

    public void setCookIngredientIsMainFlag(String cookIngredientIsMainFlag) {
        this.cookIngredientIsMainFlag = cookIngredientIsMainFlag;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public Integer getUnitId() {
        return unitId;
    }

    public void setUnitId(Integer unitId) {
        this.unitId = unitId;
    }

    public List<CreateDishIngredientSubstitutionDTO> getSubstitutions() {
        return substitutions;
    }

    public void setSubstitutions(List<CreateDishIngredientSubstitutionDTO> substitutions) {
        this.substitutions = substitutions;
    }
}


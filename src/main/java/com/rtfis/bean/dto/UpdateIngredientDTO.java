package com.rtfis.bean.dto;

import org.springframework.web.multipart.MultipartFile;

public class UpdateIngredientDTO {
    private int ingredientId;
    private int fileId;
    private MultipartFile ingredientPic;
    private String fileGroup;
    private String ingredientNameTh;
    private String ingredientNameEn;
    private String ingredientDetailTh;
    private String ingredientDetailEn;
    private Integer ingredientTypeId;

    public int getIngredientId() {
        return ingredientId;
    }

    public void setIngredientId(int ingredientId) {
        this.ingredientId = ingredientId;
    }

    public int getFileId() {
        return fileId;
    }

    public void setFileId(int fileId) {
        this.fileId = fileId;
    }

    public MultipartFile getIngredientPic() {
        return ingredientPic;
    }

    public void setIngredientPic(MultipartFile ingredientPic) {
        this.ingredientPic = ingredientPic;
    }

    public String getFileGroup() {
        return fileGroup;
    }

    public void setFileGroup(String fileGroup) {
        this.fileGroup = fileGroup;
    }

    public String getIngredientNameTh() {
        return ingredientNameTh;
    }

    public void setIngredientNameTh(String ingredientNameTh) {
        this.ingredientNameTh = ingredientNameTh;
    }

    public String getIngredientNameEn() {
        return ingredientNameEn;
    }

    public void setIngredientNameEn(String ingredientNameEn) {
        this.ingredientNameEn = ingredientNameEn;
    }

    public String getIngredientDetailTh() {
        return ingredientDetailTh;
    }

    public void setIngredientDetailTh(String ingredientDetailTh) {
        this.ingredientDetailTh = ingredientDetailTh;
    }

    public String getIngredientDetailEn() {
        return ingredientDetailEn;
    }

    public void setIngredientDetailEn(String ingredientDetailEn) {
        this.ingredientDetailEn = ingredientDetailEn;
    }

    public Integer getIngredientTypeId() {
        return ingredientTypeId;
    }

    public void setIngredientTypeId(Integer ingredientTypeId) {
        this.ingredientTypeId = ingredientTypeId;
    }
}


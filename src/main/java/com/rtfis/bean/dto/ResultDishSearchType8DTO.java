package com.rtfis.bean.dto;

import java.io.Serializable;

public class ResultDishSearchType8DTO implements Serializable {
    private String dishId;
    private String dishFileUrl;
    private String dishNameTh;
    private String dishNameEn;
    private String cookFormulaName;
    private String cookDetail;
    private String mainFileIngredient;
    private String mainIngredientTh;
    private String mainIngredientEn;
    private String cookIngredientAmount;
    private String unitNameTh;
    private String unitNameEn;
    private String mainIngredientDetailTh;
    private String mainIngredientDetailEn;
    private String subFileIngredient;
    private String subIngredientTh;
    private String subIngredientEn;
    private String subIngredientDetailTh;
    private String subIngredientDetailEn;

    public String getDishId() {
        return dishId;
    }

    public void setDishId(String dishId) {
        this.dishId = dishId;
    }

    public String getDishFileUrl() {
        return dishFileUrl;
    }

    public void setDishFileUrl(String dishFileUrl) {
        this.dishFileUrl = dishFileUrl;
    }

    public String getDishNameTh() {
        return dishNameTh;
    }

    public void setDishNameTh(String dishNameTh) {
        this.dishNameTh = dishNameTh;
    }

    public String getDishNameEn() {
        return dishNameEn;
    }

    public void setDishNameEn(String dishNameEn) {
        this.dishNameEn = dishNameEn;
    }

    public String getCookFormulaName() {
        return cookFormulaName;
    }

    public void setCookFormulaName(String cookFormulaName) {
        this.cookFormulaName = cookFormulaName;
    }

    public String getCookDetail() {
        return cookDetail;
    }

    public void setCookDetail(String cookDetail) {
        this.cookDetail = cookDetail;
    }

    public String getMainFileIngredient() {
        return mainFileIngredient;
    }

    public void setMainFileIngredient(String mainFileIngredient) {
        this.mainFileIngredient = mainFileIngredient;
    }

    public String getMainIngredientTh() {
        return mainIngredientTh;
    }

    public void setMainIngredientTh(String mainIngredientTh) {
        this.mainIngredientTh = mainIngredientTh;
    }

    public String getMainIngredientEn() {
        return mainIngredientEn;
    }

    public void setMainIngredientEn(String mainIngredientEn) {
        this.mainIngredientEn = mainIngredientEn;
    }

    public String getCookIngredientAmount() {
        return cookIngredientAmount;
    }

    public void setCookIngredientAmount(String cookIngredientAmount) {
        this.cookIngredientAmount = cookIngredientAmount;
    }

    public String getUnitNameTh() {
        return unitNameTh;
    }

    public void setUnitNameTh(String unitNameTh) {
        this.unitNameTh = unitNameTh;
    }

    public String getUnitNameEn() {
        return unitNameEn;
    }

    public void setUnitNameEn(String unitNameEn) {
        this.unitNameEn = unitNameEn;
    }

    public String getMainIngredientDetailTh() {
        return mainIngredientDetailTh;
    }

    public void setMainIngredientDetailTh(String mainIngredientDetailTh) {
        this.mainIngredientDetailTh = mainIngredientDetailTh;
    }

    public String getMainIngredientDetailEn() {
        return mainIngredientDetailEn;
    }

    public void setMainIngredientDetailEn(String mainIngredientDetailEn) {
        this.mainIngredientDetailEn = mainIngredientDetailEn;
    }

    public String getSubFileIngredient() {
        return subFileIngredient;
    }

    public void setSubFileIngredient(String subFileIngredient) {
        this.subFileIngredient = subFileIngredient;
    }

    public String getSubIngredientTh() {
        return subIngredientTh;
    }

    public void setSubIngredientTh(String subIngredientTh) {
        this.subIngredientTh = subIngredientTh;
    }

    public String getSubIngredientEn() {
        return subIngredientEn;
    }

    public void setSubIngredientEn(String subIngredientEn) {
        this.subIngredientEn = subIngredientEn;
    }

    public String getSubIngredientDetailTh() {
        return subIngredientDetailTh;
    }

    public void setSubIngredientDetailTh(String subIngredientDetailTh) {
        this.subIngredientDetailTh = subIngredientDetailTh;
    }

    public String getSubIngredientDetailEn() {
        return subIngredientDetailEn;
    }

    public void setSubIngredientDetailEn(String subIngredientDetailEn) {
        this.subIngredientDetailEn = subIngredientDetailEn;
    }

    @Override
    public String toString() {
        return "ResultDishSearchType8DTO{" +
                "dishId='" + dishId + '\'' +
                ", dishFileUrl='" + dishFileUrl + '\'' +
                ", dishNameTh='" + dishNameTh + '\'' +
                ", dishNameEn='" + dishNameEn + '\'' +
                ", cookFormulaName='" + cookFormulaName + '\'' +
                ", cookDetail='" + cookDetail + '\'' +
                ", mainFileIngredient='" + mainFileIngredient + '\'' +
                ", mainIngredientTh='" + mainIngredientTh + '\'' +
                ", mainIngredientEn='" + mainIngredientEn + '\'' +
                ", cookIngredientAmount='" + cookIngredientAmount + '\'' +
                ", unitNameTh='" + unitNameTh + '\'' +
                ", unitNameEn='" + unitNameEn + '\'' +
                ", mainIngredientDetailTh='" + mainIngredientDetailTh + '\'' +
                ", mainIngredientDetailEn='" + mainIngredientDetailEn + '\'' +
                ", subFileIngredient='" + subFileIngredient + '\'' +
                ", subIngredientTh='" + subIngredientTh + '\'' +
                ", subIngredientEn='" + subIngredientEn + '\'' +
                ", subIngredientDetailTh='" + subIngredientDetailTh + '\'' +
                ", subIngredientDetailEn='" + subIngredientDetailEn + '\'' +
                '}';
    }
}

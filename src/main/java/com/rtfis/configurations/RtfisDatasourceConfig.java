package com.rtfis.configurations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.boot.orm.jpa.EntityManagerFactoryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.naming.NamingException;
import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

@Configuration
@EnableTransactionManagement
@EnableJpaRepositories(entityManagerFactoryRef = "rtfisEntityManagerFactory", basePackages = "com.rtfis.repositories", transactionManagerRef = "rtfisTransactionManager")
public class RtfisDatasourceConfig {

    @Autowired
    private DatasourceContext datasourceContext;


    @Bean("rtfisDataSource")
    public DataSource rtfisDataSource() throws IllegalArgumentException, NamingException {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        dataSourceBuilder.driverClassName(datasourceContext.getDriverClassName());
        dataSourceBuilder.url(datasourceContext.getUrl());
        dataSourceBuilder.username(datasourceContext.getUsername());
        dataSourceBuilder.password(datasourceContext.getPassword());
        return dataSourceBuilder.build();
    }

    @Bean("rtfisEntityManagerFactory")
    public LocalContainerEntityManagerFactoryBean rtfisEntityManagerFactory(EntityManagerFactoryBuilder builder,
                                                                           DataSource rtfisDataSource) {
        return builder.dataSource(rtfisDataSource).packages("com.rtfis.entities")
                .persistenceUnit("rtfis").build();
    }

    @Bean(name = "rtfisTransactionManager")
    public PlatformTransactionManager rtfisTransactionManager(
            EntityManagerFactory rtfisEntityManagerFactory) {
        return new JpaTransactionManager(rtfisEntityManagerFactory);
    }


}
